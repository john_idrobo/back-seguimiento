package co.edu.uniajc.seguimiento.service;

import co.edu.uniajc.seguimiento.factory.ActivitiesStudentsFactory;
import co.edu.uniajc.seguimiento.model.ActivitiesStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.ActivitiesStudentsDto;
import co.edu.uniajc.seguimiento.repository.ActivitiesStudentsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivitiesStudentsServiceUnitTests {

    @Autowired
    private ActivitiesStudentsRepository activitiesStudentsRepository;

    @Autowired
    private ActivitiesStudentsService activitiesStudentsService;

    @Test
    public void testSaveActivitiesStudents() {
        assertNotNull(activitiesStudentsRepository.save(new ActivitiesStudentsFactory().setActivities((Integer) 2).setStudents("111477").newInstanceModel()));
        assertNotNull(activitiesStudentsService.saveActivitiesStudents(new ActivitiesStudentsFactory().setActivities((Integer) 3).setStudents("111478").newInstanceDto()));
    }

    @Test
    public void testUpdateActivitiesStudents() {
        List<ActivitiesStudentsModel> activitiesStudentsModelList = activitiesStudentsRepository.findAllActivitiesStudents();
        ActivitiesStudentsModel activitiesStudentsModelObjectOne = activitiesStudentsModelList.get(0);
        activitiesStudentsModelObjectOne.setStudents("111224");
        assertNotNull(activitiesStudentsRepository.save(activitiesStudentsModelObjectOne));
        List<ActivitiesStudentsDto> activitiesStudentsDtoList = activitiesStudentsService.findAllActivitiesStudents();
        ActivitiesStudentsDto activitiesStudentsDtoObjectOne = activitiesStudentsDtoList.get(0);
        activitiesStudentsDtoObjectOne.setStudents("123001");
        assertNotNull(activitiesStudentsService.updateActivitiesStudents(activitiesStudentsDtoObjectOne));
    }

    @Test
    public void findAllActivitiesStudents() {
        assertNotNull(activitiesStudentsRepository.findAllActivitiesStudents());
        assertNotNull(activitiesStudentsService.findAllActivitiesStudents());
    }

    @Test
    public void findAllActivitiesStudentsByDate() {
        assertNotNull(activitiesStudentsRepository.findAllActivitiesStudentsByDate(LocalDate.now()));
        assertNotNull(activitiesStudentsService.findAllActivitiesStudentsByDate(LocalDate.now()));
    }
}