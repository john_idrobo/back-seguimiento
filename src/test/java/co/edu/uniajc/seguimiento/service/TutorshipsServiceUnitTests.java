package co.edu.uniajc.seguimiento.service;

import co.edu.uniajc.seguimiento.factory.TutorshipsFactory;
import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import co.edu.uniajc.seguimiento.repository.TutorshipsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TutorshipsServiceUnitTests {

    @Autowired
    private TutorshipsRepository tutorshipsRepository;

    @Autowired
    private TutorshipsService tutorshipsService;

    @Test
    public void testSaveTutorships() {
        assertNotNull(tutorshipsRepository.save(new TutorshipsFactory().setUsers("114563").newInstanceModel()));
        assertNotNull(tutorshipsService.saveTutorships(new TutorshipsFactory().setUsers("114564").newInstanceDto()));
    }

    @Test
    public void testUpdateTutorships() {
        List<TutorshipsModel> tutorshipsModelList = tutorshipsRepository.findAllTutorshipsByFinished(false);
        TutorshipsModel tutorshipsModelObjectOne = tutorshipsModelList.get(0);
        tutorshipsModelObjectOne.setDescription("Prueba-1");
        assertNotNull(tutorshipsRepository.save(tutorshipsModelObjectOne));
        List<TutorshipsDto> tutorshipsDtoList = tutorshipsService.findAllTutorshipsByFinished(false);
        TutorshipsDto tutorshipsDtoObjectOne = tutorshipsDtoList.get(0);
        tutorshipsDtoObjectOne.setDescription("Prueba-2");
        assertNotNull(tutorshipsService.saveTutorships(tutorshipsDtoObjectOne));
    }

    @Test
    public void findAllTutorshipsByFinished() {
        assertNotNull(tutorshipsRepository.findAllTutorshipsByFinished(false));
        assertNotNull(tutorshipsService.findAllTutorshipsByFinished(false));
    }

    @Test
    public void findAllTutorshipsByFinishedAndDate() {
        assertNotNull(tutorshipsRepository.findAllTutorshipsByFinishedAndDate(false, LocalDate.now()));
        assertNotNull(tutorshipsService.findAllTutorshipsByFinishedAndDate(false, LocalDate.now()));
    }
}