package co.edu.uniajc.seguimiento.service;

import co.edu.uniajc.seguimiento.factory.AttendanceFactory;
import co.edu.uniajc.seguimiento.factory.ScheduleFactory;
import co.edu.uniajc.seguimiento.factory.TutorshipsFactory;
import co.edu.uniajc.seguimiento.factory.TutorshipsStudentsFactory;
import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.TutorshipsStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsStudentsDto;
import co.edu.uniajc.seguimiento.repository.AttendanceRepository;
import co.edu.uniajc.seguimiento.repository.ScheduleRepository;
import co.edu.uniajc.seguimiento.repository.TutorshipsRepository;
import co.edu.uniajc.seguimiento.repository.TutorshipsStudentsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TutorshipsStudentsServiceUnitTests {

    @Autowired
    private TutorshipsRepository tutorshipsRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private TutorshipsStudentsRepository tutorshipsStudentsRepository;

    @Autowired
    private AttendanceRepository attendanceRepository;

    @Autowired
    private TutorshipsService tutorshipsService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private TutorshipsStudentsService tutorshipsStudentsService;

    @Autowired
    private AttendanceService attendanceService;

    @Test
    public void testSaveTutorshipsStudents() {
        TutorshipsModel objectTutorshipsModel = tutorshipsRepository.save(new TutorshipsFactory().setDescription("Prueba-1").newInstanceModel());
        assertNotNull(scheduleRepository.save(new ScheduleFactory(objectTutorshipsModel.getIdTutorships(), objectTutorshipsModel.getUsers()).newInstanceModel()));
        TutorshipsStudentsModel objectTutorshipsStudentsModel = tutorshipsStudentsRepository.save(new TutorshipsStudentsFactory(objectTutorshipsModel.getIdTutorships(), objectTutorshipsModel.getUsers()).setStudents("123443").newInstanceModel());
        assertNotNull(attendanceRepository.save(new AttendanceFactory(objectTutorshipsStudentsModel.getIdTutorshipsStudents(), objectTutorshipsStudentsModel.getStudents()).newInstanceModel()));
        TutorshipsDto objectTutorshipsDto = tutorshipsService.saveTutorships(new TutorshipsFactory().setDescription("Prueba-2").newInstanceDto());
        assertNotNull(scheduleService.saveSchedule(new ScheduleFactory(objectTutorshipsDto.getIdTutorships(), objectTutorshipsDto.getUsers()).setStartTime(LocalTime.parse("09:35")).setEndTime(LocalTime.parse("10:15")).newInstanceDto()));
        TutorshipsStudentsDto objectTutorshipsStudentsDto = tutorshipsStudentsService.saveTutorshipsStudents(new TutorshipsStudentsFactory(objectTutorshipsDto.getIdTutorships(), objectTutorshipsDto.getUsers()).setStudents("123444").newInstanceDto());
    }

    @Test
    public void testUpdateTutorshipsStudents() {
        List<TutorshipsStudentsModel> tutorshipsModelList = tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinished(false);
        TutorshipsStudentsModel tutorshipsStudentsModelObject = tutorshipsModelList.get(0);
        tutorshipsStudentsModelObject.setStudents("296516");
        assertNotNull(tutorshipsStudentsRepository.save(tutorshipsStudentsModelObject));
        List<TutorshipsStudentsDto> tutorshipsDtoList = tutorshipsStudentsService.findAllTutorshipsStudentsByFinished(false);
        TutorshipsStudentsDto tutorshipsStudentsDtoObject = tutorshipsDtoList.get(0);
        tutorshipsStudentsDtoObject.setStudents("296517");
        assertNotNull(tutorshipsStudentsService.updateTutorshipsStudents(tutorshipsStudentsDtoObject));
    }

    @Test
    public void findAllTutorshipsStudentsByFinished() {
        assertNotNull(tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinished(false));
        assertNotNull(tutorshipsStudentsService.findAllTutorshipsStudentsByFinished(false));
    }
}