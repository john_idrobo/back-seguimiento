package co.edu.uniajc.seguimiento.service;

import co.edu.uniajc.seguimiento.factory.PsychologicalStudentsFactory;
import co.edu.uniajc.seguimiento.model.PsychologicalStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.PsychologicalStudentsDto;
import co.edu.uniajc.seguimiento.repository.PsychologicalStudentsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PsychologicalStudentsServiceUnitTests {

    @Autowired
    private PsychologicalStudentsRepository psychologicalStudentsRepository;

    @Autowired
    private PsychologicalStudentsService psychologicalStudentsService;

    @Test
    public void testSavePsychologicalStudents() {
        assertNotNull(psychologicalStudentsRepository.save(new PsychologicalStudentsFactory().setUsers("114555").newInstanceModel()));
        assertNotNull(psychologicalStudentsService.savePsychologicalStudents(new PsychologicalStudentsFactory().setUsers("114556").newInstanceDto()));
    }

    @Test
    public void testUpdatePsychologicalStudents() {
        PsychologicalStudentsModel psychologicalStudentsModelList = psychologicalStudentsRepository.save(new PsychologicalStudentsFactory().setUsers("114536").newInstanceModel());
        psychologicalStudentsModelList.setStudents("113550");
        assertNotNull(psychologicalStudentsRepository.save(psychologicalStudentsModelList));
        List<PsychologicalStudentsDto> psychologicalStudentsDtoList = psychologicalStudentsService.findAllPsychologicalStudents();
        PsychologicalStudentsDto psychologicalStudentsDtoObjectOne = psychologicalStudentsDtoList.get(0);
        psychologicalStudentsDtoObjectOne.setStudents("113551");
        assertNotNull(psychologicalStudentsService.updatePsychologicalStudents(psychologicalStudentsDtoObjectOne));
    }

    @Test
    public void findAllPsychologicalStudents() {
        assertNotNull(psychologicalStudentsRepository.save(new PsychologicalStudentsFactory().setUsers("1145444").newInstanceModel()));
        assertNotNull(psychologicalStudentsRepository.findAllPsychologicalStudents());
        assertNotNull(psychologicalStudentsService.savePsychologicalStudents(new PsychologicalStudentsFactory().setUsers("114333").newInstanceDto()));
        assertNotNull(psychologicalStudentsService.findAllPsychologicalStudents());
    }

    @Test
    public void findAllPsychologicalStudentsByDate() {
        assertNotNull(psychologicalStudentsRepository.findAllPsychologicalStudentsByDate(LocalDate.now()));
        assertNotNull(psychologicalStudentsService.findAllPsychologicalStudentsByDate(LocalDate.now()));
    }
}