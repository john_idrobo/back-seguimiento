package co.edu.uniajc.seguimiento.factory;

import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;

public class TutorshipsFactory {

    private final Integer idTutorships;
    private final String timeZone;
    private String description;
    private String users;

    public TutorshipsFactory() {
        this.idTutorships = 1;
        this.timeZone = "Norte-Tarde";
        this.description = "Tutoría refuerzo cálculo";
        this.users = "114550";
    }

    public TutorshipsModel newInstanceModel() {
        return TutorshipsModel.builder()
                .idTutorships(this.idTutorships)
                .timeZone(this.timeZone)
                .description(this.description)
                .users(this.users)
                .build();
    }

    public TutorshipsDto newInstanceDto() {
        return TutorshipsDto.builder()
                .idTutorships(this.idTutorships)
                .timeZone(this.timeZone)
                .description(this.description)
                .users(this.users)
                .build();
    }

    public TutorshipsFactory setDescription(String description) {
        this.description = description;
        return this;
    }

    public TutorshipsFactory setUsers(String users) {
        this.users = users;
        return this;
    }
}