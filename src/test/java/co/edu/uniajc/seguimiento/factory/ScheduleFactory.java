package co.edu.uniajc.seguimiento.factory;

import co.edu.uniajc.seguimiento.model.ScheduleModel;
import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.dto.ScheduleDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;

import java.time.LocalTime;

public class ScheduleFactory {

    private final Integer idSchedule;
    private LocalTime startTime;
    private  LocalTime endTime;
    private final Integer tutorships;
    private final String users;
    private String weekday;

    public ScheduleFactory(Integer tutorships, String users) {
        this.idSchedule = 1;
        this.weekday = "Lunes";
        this.startTime = LocalTime.parse("07:15");
        this.endTime = LocalTime.parse("09:00");
        this.tutorships = tutorships;
        this.users = users;
    }

    public ScheduleModel newInstanceModel() {
        return ScheduleModel.builder()
                .idSchedule(this.idSchedule)
                .weekday(this.weekday)
                .startTime(this.startTime)
                .endTime(this.endTime)
                .tutorships(TutorshipsModel.builder()
                        .idTutorships(this.tutorships)
                        .users(this.users)
                        .build())
                .build();
    }

    public ScheduleDto newInstanceDto() {
        return ScheduleDto.builder()
                .idSchedule(this.idSchedule)
                .weekday(this.weekday)
                .startTime(this.startTime)
                .endTime(this.endTime)
                .tutorships(TutorshipsDto.builder()
                        .idTutorships(this.tutorships)
                        .users(this.users)
                        .build())
                .build();
    }

    public ScheduleFactory setStartTime(LocalTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public ScheduleFactory setEndTime(LocalTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public ScheduleFactory setWeekday(String weekday) {
        this.weekday = weekday;
        return this;
    }
}