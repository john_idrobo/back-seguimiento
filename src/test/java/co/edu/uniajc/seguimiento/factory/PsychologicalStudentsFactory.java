package co.edu.uniajc.seguimiento.factory;

import co.edu.uniajc.seguimiento.model.PsychologicalStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.PsychologicalStudentsDto;

public class PsychologicalStudentsFactory {

    private final Integer idPsychologicalStudents;
    private final String reason;
    private final String description;
    private final String factors;
    private final String familyBackground;
    private final String diagnosis;
    private String users;
    private String students;

    public PsychologicalStudentsFactory() {
        this.idPsychologicalStudents = 1;
        this.reason = "Problemas académicos";
        this.description = "Falta de atención en clases";
        this.factors = "Déficit de atención";
        this.familyBackground = "Sin antecedentes";
        this.diagnosis = "Controles periodicos por parte de un tutor";
        this.users = "114550";
        this.students = "111479";

    }

    public PsychologicalStudentsModel newInstanceModel() {
        return PsychologicalStudentsModel.builder()
                .idPsychologicalStudents(this.idPsychologicalStudents)
                .reason(this.reason)
                .description(this.description)
                .factors(this.factors)
                .familyBackground(this.familyBackground)
                .diagnosis(this.diagnosis)
                .users(this.users)
                .students(this.students)
                .build();
    }

    public PsychologicalStudentsDto newInstanceDto() {
        return PsychologicalStudentsDto.builder()
                .idPsychologicalStudents(this.idPsychologicalStudents)
                .reason(this.reason)
                .description(this.description)
                .factors(this.factors)
                .familyBackground(this.familyBackground)
                .diagnosis(this.diagnosis)
                .users(this.users)
                .students(this.students)
                .build();
    }

    public PsychologicalStudentsFactory setUsers(String users) {
        this.users = users;
        return this;
    }

    public PsychologicalStudentsFactory setStudents(String students) {
        this.students = students;
        return this;
    }
}