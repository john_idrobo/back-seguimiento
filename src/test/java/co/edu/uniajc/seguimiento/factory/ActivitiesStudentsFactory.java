package co.edu.uniajc.seguimiento.factory;

import co.edu.uniajc.seguimiento.model.ActivitiesStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.ActivitiesStudentsDto;

import java.time.LocalDate;

public class ActivitiesStudentsFactory {

    private final Integer idActivitiesStudents;
    private final LocalDate attendanceDate;
    private final Double completedHours;
    private Integer activities;
    private String students;

    public ActivitiesStudentsFactory() {
        this.idActivitiesStudents = 1;
        this.attendanceDate = LocalDate.now();
        this.completedHours = 3.5;
        this.activities = 1;
        this.students = "111479";
    }

    public ActivitiesStudentsModel newInstanceModel() {
        return ActivitiesStudentsModel.builder()
                .idActivitiesStudents(this.idActivitiesStudents)
                .attendanceDate(this.attendanceDate)
                .completedHours(this.completedHours)
                .activities(this.activities)
                .students(this.students)
                .build();
    }

    public ActivitiesStudentsDto newInstanceDto() {
        return ActivitiesStudentsDto.builder()
                .idActivitiesStudents(this.idActivitiesStudents)
                .attendanceDate(this.attendanceDate)
                .completedHours(this.completedHours)
                .activities(this.activities)
                .students(this.students)
                .build();
    }

    public ActivitiesStudentsFactory setActivities(Integer activities) {
        this.activities = activities;
        return this;
    }

    public ActivitiesStudentsFactory setStudents(String students) {
        this.students = students;
        return this;
    }
}