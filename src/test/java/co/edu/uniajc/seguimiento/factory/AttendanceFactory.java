package co.edu.uniajc.seguimiento.factory;

import co.edu.uniajc.seguimiento.model.AttendanceModel;
import co.edu.uniajc.seguimiento.model.TutorshipsStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.AttendanceDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsStudentsDto;

import java.time.LocalDateTime;

public class AttendanceFactory {

    private final Integer idAttendance;
    private final Integer tutorshipsStudents;
    private final String students;
    private final Boolean attended;
    private final LocalDateTime attendanceDateTime;

    public AttendanceFactory(Integer tutorshipsStudents, String students) {
        this.idAttendance = 1;
        this.attendanceDateTime = LocalDateTime.now();
        this.attended = true;
        this.tutorshipsStudents = tutorshipsStudents;
        this.students = students;
    }

    public AttendanceModel newInstanceModel() {
        return AttendanceModel.builder()
                .idAttendance(this.idAttendance)
                .attendanceDateTime(this.attendanceDateTime)
                .attended(this.attended)
                .tutorshipsStudents(TutorshipsStudentsModel.builder()
                        .idTutorshipsStudents(this.tutorshipsStudents)
                        .students(this.students)
                        .build())
                .build();
    }

    public AttendanceDto newInstanceDto() {
        return AttendanceDto.builder()
                .idAttendance(this.idAttendance)
                .attendanceDateTime(this.attendanceDateTime)
                .attended(this.attended)
                .tutorshipsStudents(TutorshipsStudentsDto.builder()
                        .idTutorshipsStudents(this.tutorshipsStudents)
                        .students(this.students)
                        .build())
                .build();
    }
}