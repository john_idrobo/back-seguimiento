package co.edu.uniajc.seguimiento.factory;

import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.TutorshipsStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsStudentsDto;

public class TutorshipsStudentsFactory {

    private final Integer idTutorshipsStudents;
    private final Double noteOne;
    private final Double noteTwo;
    private final Integer tutorships;
    private final String users;
    private final String subjects;
    private final String group;
    private String students;


    public TutorshipsStudentsFactory(Integer tutorships, String users) {
        this.idTutorshipsStudents = 1;
        this.noteOne = 3.5;
        this.noteTwo = 4.4;
        this.tutorships = tutorships;
        this.users = users;
        this.students = "111479";
        this.subjects = "CB012102";
        this.group = "211B";
    }

    public TutorshipsStudentsModel newInstanceModel() {
        return TutorshipsStudentsModel.builder()
                .idTutorshipsStudents(this.idTutorshipsStudents)
                .noteOne(this.noteOne)
                .noteTwo(this.noteTwo)
                .tutorships(TutorshipsModel.builder()
                        .idTutorships(this.tutorships)
                        .users(this.users)
                        .build())
                .students(this.students)
                .subjects(this.subjects)
                .group(this.group)
                .build();
    }

    public TutorshipsStudentsDto newInstanceDto() {
        return TutorshipsStudentsDto.builder()
                .idTutorshipsStudents(this.idTutorshipsStudents)
                .noteOne(this.noteOne)
                .noteTwo(this.noteTwo)
                .tutorships(TutorshipsDto.builder()
                        .idTutorships(this.tutorships)
                        .users(this.users)
                        .build())
                .students(this.students)
                .subjects(this.subjects)
                .group(this.group)
                .build();
    }

    public TutorshipsStudentsFactory setStudents(String students) {
        this.students = students;
        return this;
    }
}