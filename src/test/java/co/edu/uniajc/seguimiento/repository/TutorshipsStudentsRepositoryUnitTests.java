package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.factory.TutorshipsFactory;
import co.edu.uniajc.seguimiento.factory.TutorshipsStudentsFactory;
import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.TutorshipsStudentsModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class TutorshipsStudentsRepositoryUnitTests {

    @Autowired
    private TutorshipsRepository tutorshipsRepository;

    @Autowired
    private TutorshipsStudentsRepository tutorshipsStudentsRepository;

    @Test
    public void testSaveTutorshipsStudents() {
        TutorshipsModel tutorshipsModelObjectOne = tutorshipsRepository.save(new TutorshipsFactory().newInstanceModel());
        assertNotNull(tutorshipsStudentsRepository.save(new TutorshipsStudentsFactory(tutorshipsModelObjectOne.getIdTutorships(), tutorshipsModelObjectOne.getUsers()).newInstanceModel()));
    }

    @Test
    public void testUpdateTutorshipsStudents() {
        TutorshipsModel tutorshipsModelObjectOne = tutorshipsRepository.save(new TutorshipsFactory().newInstanceModel());
        tutorshipsStudentsRepository.save(new TutorshipsStudentsFactory(tutorshipsModelObjectOne.getIdTutorships(), tutorshipsModelObjectOne.getUsers()).newInstanceModel());
        List<TutorshipsStudentsModel> tutorshipsStudentsModelList = tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinished(false);
        TutorshipsStudentsModel tutorshipsStudentsModelObjectOne = tutorshipsStudentsModelList.get(0);
        tutorshipsStudentsModelObjectOne.setStudents("1125486");
        assertNotNull(tutorshipsStudentsRepository.save(tutorshipsStudentsModelObjectOne));
    }

    @Test
    public void findAllTutorshipsStudentsByFinished() {
        assertNotNull(tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinished(false));
    }
}