package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.factory.ActivitiesStudentsFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ActivitiesStudentsRepositoryUnitTests {

    @Autowired
    private ActivitiesStudentsRepository activitiesStudentsRepository;

    @Test
    public void testSaveActivitiesStudents() {
        assertNotNull(activitiesStudentsRepository.save(new ActivitiesStudentsFactory().newInstanceModel()));
    }

    @Test
    public void testUpdateActivitiesStudents() {
        assertNotNull(activitiesStudentsRepository.save(new ActivitiesStudentsFactory().setStudents("113564").newInstanceModel()));
    }

    @Test
    public void findAllActivitiesStudents() {
        assertNotNull(activitiesStudentsRepository.findAllActivitiesStudents());
    }

    @Test
    public void findAllActivitiesStudentsByDate() {
        assertNotNull(activitiesStudentsRepository.findAllActivitiesStudentsByDate(LocalDate.now()));
    }
}