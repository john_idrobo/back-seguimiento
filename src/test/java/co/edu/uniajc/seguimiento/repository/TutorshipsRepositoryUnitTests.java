package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.factory.TutorshipsFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class TutorshipsRepositoryUnitTests {

    @Autowired
    private TutorshipsRepository tutorshipsRepository;

    @Test
    public void testSaveTutorships() {
        assertNotNull(tutorshipsRepository.save(new TutorshipsFactory().newInstanceModel()));
    }

    @Test
    public void testUpdateTutorships() {
        assertNotNull(tutorshipsRepository.save(new TutorshipsFactory().setUsers("113456").newInstanceModel()));
    }

    @Test
    public void findAllTutorshipsByFinished() {
        assertNotNull(tutorshipsRepository.findAllTutorshipsByFinished(false));
    }

    @Test
    public void findAllTutorshipsByFinishedAndDate() {
        assertNotNull(tutorshipsRepository.findAllTutorshipsByFinishedAndDate(false, LocalDate.now()));
    }
}