package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.factory.PsychologicalStudentsFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PsychologicalStudentsRepositoryUnitTests {

    @Autowired
    private PsychologicalStudentsRepository psychologicalStudentsRepository;

    @Test
    public void testSavePsychologicalStudents() {
        assertNotNull(psychologicalStudentsRepository.save(new PsychologicalStudentsFactory().newInstanceModel()));
    }

    @Test
    public void testUpdatePsychologicalStudents() {
        assertNotNull(psychologicalStudentsRepository.save(new PsychologicalStudentsFactory().setStudents("113456").newInstanceModel()));
    }

    @Test
    public void findAllPsychologicalStudents() {
        assertNotNull(psychologicalStudentsRepository.findAllPsychologicalStudents());
    }

    @Test
    public void findAllPsychologicalStudentsByDate() {
        assertNotNull(psychologicalStudentsRepository.findAllPsychologicalStudentsByDate(LocalDate.parse("2021-08-10")));
    }
}