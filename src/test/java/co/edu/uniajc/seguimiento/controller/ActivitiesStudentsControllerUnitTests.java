package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.factory.ActivitiesStudentsFactory;
import co.edu.uniajc.seguimiento.model.dto.ActivitiesStudentsDto;
import co.edu.uniajc.seguimiento.service.ActivitiesStudentsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivitiesStudentsControllerUnitTests {

    @Autowired
    private ActivitiesStudentsService activitiesStudentsService;

    @Autowired
    private ActivitiesStudentsController activitiesStudentsController;

    @Test
    public void testSaveActivitiesStudents() {
        assertNotNull(activitiesStudentsService.saveActivitiesStudents(new ActivitiesStudentsFactory().setActivities((Integer) 2).setStudents("111477").newInstanceDto()));
        assertNotNull(activitiesStudentsController.saveActivitiesStudents(new ActivitiesStudentsFactory().setActivities((Integer) 3).setStudents("111499").newInstanceDto()));
    }

    @Test
    public void testUpdateActivitiesStudents() {
        assertNotNull(activitiesStudentsService.saveActivitiesStudents(new ActivitiesStudentsFactory().setActivities(4).setStudents("111441").newInstanceDto()));
        List<ActivitiesStudentsDto> activitiesStudentsDtoList = activitiesStudentsService.findAllActivitiesStudents();
        ActivitiesStudentsDto activitiesStudentsDtoObjectOne = activitiesStudentsDtoList.get(0);
        activitiesStudentsDtoObjectOne.setStudents("111555");
        assertNotNull(activitiesStudentsService.updateActivitiesStudents(activitiesStudentsDtoObjectOne));
        ActivitiesStudentsDto activitiesStudentsDtoObjectTwo = activitiesStudentsDtoList.get(0);
        activitiesStudentsDtoObjectTwo.setStudents("111556");
        assertNotNull(activitiesStudentsController.updateActivitiesStudents(activitiesStudentsDtoObjectTwo));
    }

    @Test
    public void findAllActivitiesStudents() {
        assertNotNull(activitiesStudentsService.findAllActivitiesStudents());
        assertNotNull(activitiesStudentsController.findAllActivitiesStudents());
    }

    @Test
    public void findAllActivitiesStudentsByDate() {
        assertNotNull(activitiesStudentsService.findAllActivitiesStudentsByDate(LocalDate.now()));
        assertNotNull(activitiesStudentsController.findAllActivitiesStudentsByDate(LocalDate.now()));
    }

}