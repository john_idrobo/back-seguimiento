package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.factory.AttendanceFactory;
import co.edu.uniajc.seguimiento.factory.ScheduleFactory;
import co.edu.uniajc.seguimiento.factory.TutorshipsFactory;
import co.edu.uniajc.seguimiento.factory.TutorshipsStudentsFactory;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsStudentsDto;
import co.edu.uniajc.seguimiento.service.AttendanceService;
import co.edu.uniajc.seguimiento.service.ScheduleService;
import co.edu.uniajc.seguimiento.service.TutorshipsService;
import co.edu.uniajc.seguimiento.service.TutorshipsStudentsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TutorshipsStudentsControllerUnitTests {

    @Autowired
    private TutorshipsService tutorshipsService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private TutorshipsStudentsService tutorshipsStudentsService;

    @Autowired
    private AttendanceService attendanceService;

    @Autowired
    private TutorshipsController tutorshipsController;

    @Autowired
    private ScheduleController scheduleController;

    @Autowired
    private TutorshipsStudentsController tutorshipsStudentsController;

    @Autowired
    private AttendanceController attendanceController;

    @Test
    public void testSaveTutorshipsStudents() {
        TutorshipsDto objectTutorshipsDtoOne = tutorshipsService.saveTutorships(new TutorshipsFactory().setDescription("Prueba-3").newInstanceDto());
        assertNotNull(scheduleService.saveSchedule(new ScheduleFactory(objectTutorshipsDtoOne.getIdTutorships(), objectTutorshipsDtoOne.getUsers()).setStartTime(LocalTime.now().minusMinutes(50)).setEndTime(LocalTime.now().plusHours(1)).newInstanceDto()));
        assertNotNull(tutorshipsStudentsService.saveTutorshipsStudents(new TutorshipsStudentsFactory(objectTutorshipsDtoOne.getIdTutorships(), objectTutorshipsDtoOne.getUsers()).setStudents("296517").newInstanceDto()));
        assertNotNull(tutorshipsController.saveTutorships(new TutorshipsFactory().setDescription("Prueba-4").newInstanceDto()));
        assertNotNull(scheduleController.saveSchedule(new ScheduleFactory(3, "114550").setStartTime(LocalTime.now().minusHours(2)).setEndTime(LocalTime.now().minusHours(1)).newInstanceDto()));
        assertNotNull(tutorshipsStudentsController.saveTutorshipsStudents(new TutorshipsStudentsFactory(3, "114550").setStudents("123445").newInstanceDto()));
    }

    @Test
    public void testUpdateTutorshipsStudents() {
        List<TutorshipsStudentsDto> tutorshipsStudentsDtoList = tutorshipsStudentsService.findAllTutorshipsStudentsByFinished(false);
        TutorshipsStudentsDto tutorshipsStudentsDtoObjectOne = tutorshipsStudentsDtoList.get(0);
        tutorshipsStudentsDtoObjectOne.setSubjects("CB02456");
        assertNotNull(tutorshipsStudentsService.updateTutorshipsStudents(tutorshipsStudentsDtoObjectOne));
        TutorshipsStudentsDto tutorshipsStudentsDtoObjectTwo = tutorshipsStudentsDtoList.get(0);
        tutorshipsStudentsDtoObjectTwo.setSubjects("CB02457");
        assertNotNull(tutorshipsStudentsController.updateTutorshipsStudents(tutorshipsStudentsDtoObjectTwo));
    }

    @Test
    public void findAllTutorshipsStudentsByFinished() {
        assertNotNull(tutorshipsStudentsService.findAllTutorshipsStudentsByFinished(false));
        assertNotNull(tutorshipsStudentsController.findAllTutorshipsStudentsByFinished(false));
    }
}