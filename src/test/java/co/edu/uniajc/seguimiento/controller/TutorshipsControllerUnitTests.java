package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.factory.TutorshipsFactory;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import co.edu.uniajc.seguimiento.service.TutorshipsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TutorshipsControllerUnitTests {

    @Autowired
    private TutorshipsService tutorshipsService;

    @Autowired
    private TutorshipsController tutorshipsController;

    @Test
    public void testSaveTutorships() {
        assertNotNull(tutorshipsService.saveTutorships(new TutorshipsFactory().setDescription("Prueba-1").setUsers("113566").newInstanceDto()));
        assertNotNull(tutorshipsController.saveTutorships(new TutorshipsFactory().setDescription("Prueba-2").setUsers("113567").newInstanceDto()));
    }

    @Test
    public void testUpdateTutorships() {
        List<TutorshipsDto> tutorshipsDtoList = tutorshipsService.findAllTutorshipsByFinished(false);
        TutorshipsDto tutorshipsDtoObjectOne = tutorshipsDtoList.get(0);
        tutorshipsDtoObjectOne.setDescription("Prueba-3");
        assertNotNull(tutorshipsService.saveTutorships(tutorshipsDtoObjectOne));
        TutorshipsDto tutorshipsDtoObjectTwo = tutorshipsDtoList.get(0);
        tutorshipsDtoObjectTwo.setDescription("Prueba-4");
        assertNotNull(tutorshipsController.updateTutorships(tutorshipsDtoObjectTwo));
    }

    @Test
    public void findAllTutorshipsByFinished() {
        assertNotNull(tutorshipsService.findAllTutorshipsByFinished(false));
        assertNotNull(tutorshipsController.findAllTutorshipsByFinished(false));
    }

    @Test
    public void findAllTutorshipsByFinishedAndDate() {
        assertNotNull(tutorshipsService.findAllTutorshipsByFinishedAndDate(false, LocalDate.now()));
        assertNotNull(tutorshipsController.findAllTutorshipsByFinishedAndDate(false, LocalDate.now()));
    }
}