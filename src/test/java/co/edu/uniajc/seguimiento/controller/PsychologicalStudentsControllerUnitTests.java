package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.factory.PsychologicalStudentsFactory;
import co.edu.uniajc.seguimiento.model.dto.PsychologicalStudentsDto;
import co.edu.uniajc.seguimiento.service.PsychologicalStudentsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PsychologicalStudentsControllerUnitTests {

    @Autowired
    private PsychologicalStudentsService psychologicalStudentsService;

    @Autowired
    private PsychologicalStudentsController psychologicalStudentsController;

    @Test
    public void testSavePsychologicalStudents() {
        assertNotNull(psychologicalStudentsService.savePsychologicalStudents(new PsychologicalStudentsFactory().setStudents("112551").newInstanceDto()));
        assertNotNull(psychologicalStudentsController.savePsychologicalStudents(new PsychologicalStudentsFactory().setStudents("113552").setUsers("111358").newInstanceDto()));
    }

    @Test
    public void testUpdatePsychologicalStudents() {
        List<PsychologicalStudentsDto> psychologicalStudentsDtoList = psychologicalStudentsService.findAllPsychologicalStudents();
        PsychologicalStudentsDto psychologicalStudentsDtoObjectOne = psychologicalStudentsDtoList.get(0);
        psychologicalStudentsDtoObjectOne.setStudents("113449");
        assertNotNull(psychologicalStudentsService.updatePsychologicalStudents(psychologicalStudentsDtoObjectOne));
        PsychologicalStudentsDto psychologicalStudentsDtoObjectTwo = psychologicalStudentsDtoList.get(0);
        psychologicalStudentsDtoObjectTwo.setStudents("113448");
        assertNotNull(psychologicalStudentsController.updatePsychologicalStudents(psychologicalStudentsDtoObjectTwo));
    }

    @Test
    public void findAllPsychologicalStudents() {
        assertNotNull(psychologicalStudentsService.findAllPsychologicalStudents());
        assertNotNull(psychologicalStudentsController.findAllPsychologicalStudents());
    }

    @Test
    public void findAllPsychologicalStudentsByDate() {
        assertNotNull(psychologicalStudentsService.findAllPsychologicalStudentsByDate(LocalDate.now()));
        assertNotNull(psychologicalStudentsController.findAllPsychologicalStudentsByDate(LocalDate.now()));
    }
}