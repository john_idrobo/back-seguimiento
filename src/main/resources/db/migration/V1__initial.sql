--*****************************Database Seguimiento********************************************************
CREATE TABLE "tutorships"
(
    tu_id            serial                 NOT NULL,
    tu_creation_date date                   NOT NULL,
    tu_time_zone     character varying(100) NOT NULL,
    tu_description   character varying(500) NOT NULL,
    tu_finished      boolean                NOT NULL,
    tu_users         character varying(10)  NOT NULL,
    tu_active        boolean                NOT NULL,
    PRIMARY KEY (tu_id)
);

ALTER TABLE "tutorships"
    ALTER COLUMN tu_creation_date SET DEFAULT CURRENT_DATE;

ALTER TABLE "tutorships"
    ALTER COLUMN tu_finished SET DEFAULT FALSE;

ALTER TABLE "tutorships"
    ALTER COLUMN tu_active SET DEFAULT TRUE;

ALTER TABLE "tutorships"
    OWNER to postgres;

CREATE TABLE "schedule"
(
    sc_id            serial                NOT NULL,
    sc_creation_date date                  NOT NULL,
    sc_weekday       character varying(10) NOT NULL,
    sc_start_time    time                  NOT NULL,
    sc_end_time      time                  NOT NULL,
    sc_tutorships    integer               NOT NULL,
    sc_active        boolean               NOT NULL,
    PRIMARY KEY (sc_id)
);

ALTER TABLE "schedule"
    add constraint sc_tutorships
        foreign key (sc_tutorships)
            references "tutorships" (tu_id);

ALTER TABLE "schedule"
    ALTER COLUMN sc_creation_date SET DEFAULT CURRENT_DATE;

ALTER TABLE "schedule"
    ALTER COLUMN sc_active SET DEFAULT TRUE;

ALTER TABLE "schedule"
    OWNER to postgres;

CREATE TABLE "tutorships_students"
(
    ts_id            serial                NOT NULL,
    ts_creation_date date                  NOT NULL,
    ts_note_one      double precision      NOT NULL,
    ts_note_two      double precision      NOT NULL,
    ts_tutorships    integer               NOT NULL,
    ts_subjects      character varying(10) NOT NULL,
    ts_group         character varying(10) NOT NULL,
    ts_students      character varying(10) NOT NULL,
    ts_active        boolean               NOT NULL,
    PRIMARY KEY (ts_id)
);

ALTER TABLE "tutorships_students"
    add constraint ts_tutorships
        foreign key (ts_tutorships)
            references "tutorships" (tu_id);

ALTER TABLE "tutorships_students"
    ALTER COLUMN ts_creation_date SET DEFAULT CURRENT_DATE;

ALTER TABLE "tutorships_students"
    ALTER COLUMN ts_active SET DEFAULT TRUE;

ALTER TABLE "tutorships_students"
    OWNER to postgres;

CREATE TABLE "attendance"
(
    at_id                   serial    NOT NULL,
    at_creation_date        date      NOT NULL,
    at_attendance_date_time timestamp NOT NULL,
    at_attended             boolean   NOT NULL,
    at_tutorships_students  integer   NOT NULL,
    at_active               boolean   NOT NULL,
    PRIMARY KEY (at_id)
);

ALTER TABLE "attendance"
    add constraint at_tutorships_students
        foreign key (at_tutorships_students)
            references "tutorships_students" (ts_id);

ALTER TABLE "attendance"
    ALTER COLUMN at_creation_date SET DEFAULT CURRENT_DATE;

ALTER TABLE "attendance"
    ALTER COLUMN at_active SET DEFAULT TRUE;

ALTER TABLE "attendance"
    OWNER to postgres;

CREATE TABLE "psychological_students"
(
    ps_id                serial                 NOT NULL,
    ps_creation_date     date                   NOT NULL,
    ps_reason            character varying(500) NOT NULL,
    ps_description       character varying(500) NOT NULL,
    ps_factors           character varying(500) NOT NULL,
    ps_family_background character varying(500) NOT NULL,
    ps_diagnosis         character varying(500) NOT NULL,
    ps_users             character varying(10)  NOT NULL,
    ps_students          character varying(10)  NOT NULL,
    ps_active            boolean                NOT NULL,
    PRIMARY KEY (ps_id)
);

ALTER TABLE "psychological_students"
    ALTER COLUMN ps_creation_date SET DEFAULT CURRENT_DATE;

ALTER TABLE "psychological_students"
    ALTER COLUMN ps_active SET DEFAULT TRUE;

ALTER TABLE "psychological_students"
    OWNER to postgres;

CREATE TABLE "activities_students"
(
    as_id              serial                NOT NULL,
    as_creation_date   date                  NOT NULL,
    as_attendance_date date                  NOT NULL,
    as_completed_hours double precision      NOT NULL,
    as_activities      integer               NOT NULL,
    as_students        character varying(10) NOT NULL,
    as_active          boolean               NOT NULL,
    PRIMARY KEY (as_id)
);

ALTER TABLE "activities_students"
    ALTER COLUMN as_creation_date SET DEFAULT CURRENT_DATE;

ALTER TABLE "activities_students"
    ALTER COLUMN as_active SET DEFAULT TRUE;

ALTER TABLE "activities_students"
    OWNER to postgres;
--*********************************************************************************************************
--*******Función Inactivar Tutoría, Schedule y Tutoría-Estudiante Por Id****************************************************
CREATE
OR REPLACE FUNCTION inactiveTutorshipsById(id integer) RETURNS integer AS $$
DECLARE
data integer;
BEGIN
UPDATE tutorships
SET tu_active='false'
WHERE tu_id = id RETURNING tu_id
INTO data;
IF
(data>0)THEN
UPDATE schedule
SET sc_active='false'
WHERE sc_tutorships = id;
UPDATE attendance a
SET at_active='false' FROM tutorships_students b,
tutorships c
WHERE
    a.at_tutorships_students=b.ts_id
  AND
    b.ts_tutorships= c.tu_id
  AND
    c.tu_id = id;
UPDATE tutorships_students
SET ts_active='false'
WHERE ts_tutorships = id;
RETURN 1;
ELSE
RETURN 0;
END IF;
END;
$$
LANGUAGE plpgsql;
--*********************************************************************************************************