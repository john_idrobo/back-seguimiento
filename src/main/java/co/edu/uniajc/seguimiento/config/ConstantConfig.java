package co.edu.uniajc.seguimiento.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "constant")
@PropertySource("classpath:messages/validation/message_es_CO.properties")
@Setter
@Getter
public class ConstantConfig {
    //Constant Generic
    private String ok;
    private String created;
    private String createdUpdate;
    private String createdInactive;
    private String createdFinished;
    private String badRequest;
    private String forbidden;
    private String internalError;
    private String errorStructure;
    private String notFound;
    private String unauthorized;
    private String fetch;
    //Constant ActivitiesStudents
    private String notActivities;
    private String badActivities;
    private String inactiveActivities;
    //Constant PsychologicalStudents
    private String notPsychological;
    private String badPsychological;
    private String inactivePsychological;
    //Constant Tutorships
    private String notTutorships;
    private String badTutorships;
    private String users;
    private String inactiveTutorships;
    private String finishedTutorships;
    //Constant Schedule
    private String notSchedule;
    private String badSchedule;
    private String equal;
    private String before;
    private String duration;
    private String limit;
    private String crossingSchedule;
    private String inactiveSchedule;
    //Constant TutorshipsStudents
    private String notStudents;
    private String badStudents;
    private String inactiveStudents;
    private String badTutorshipsStudents;
    //Constant Attendance
    private String notAttendance;
    private String badAttendance;
    private String inactiveAttendance;
    private String limitAttendance;
}