package co.edu.uniajc.seguimiento.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class DayOfWeekConfig {

    public String convertDayOfWeek(String day) {
        String dayString;
        switch (day) {
            case "MONDAY":
                dayString = "Lunes";
                break;
            case "TUESDAY":
                dayString = "Martes";
                break;
            case "WEDNESDAY":
                dayString = "Miercoles";
                break;
            case "THURSDAY":
                dayString = "Jueves";
                break;
            case "FRIDAY":
                dayString = "Viernes";
                break;
            case "SATURDAY":
                dayString = "Sabado";
                break;
            case "SUNDAY":
                dayString = "Domingo";
                break;
            default:
                dayString = "Dia invalido";
                break;
        }
        return dayString;
    }
}