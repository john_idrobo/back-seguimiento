package co.edu.uniajc.seguimiento.config;

import co.edu.uniajc.seguimiento.utility.exception.*;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectMessage;
import org.hibernate.exception.GenericJDBCException;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.persistence.RollbackException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.time.DateTimeException;
import java.util.stream.Collectors;


@Configuration
@ControllerAdvice(annotations = {RestController.class})
public class ExceptionConfig extends ResponseEntityExceptionHandler {

    @Autowired
    private ConstantConfig constantConfig;

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<Object> badRequestException(BadRequestException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.BAD_REQUEST.value(), constantConfig.getBadRequest(), ex.getMessage(), request.getRequestURI()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> constraintViolationException(ConstraintViolationException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.BAD_REQUEST.value(), constantConfig.getBadRequest(), ex.getConstraintViolations().stream().map(ConstraintViolation::getMessageTemplate).collect(Collectors.toList()), request.getRequestURI()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SQLGrammarException.class)
    public ResponseEntity<Object> sqlGrammarException(SQLGrammarException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.BAD_REQUEST.value(), constantConfig.getErrorStructure(), ex.getSQLException().getMessage(), request.getRequestURI()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<Object> nullPointerException(NullPointerException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.BAD_REQUEST.value(), constantConfig.getBadRequest(), ex.getMessage(), request.getRequestURI()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<Object> internalServerErrorException(InternalServerErrorException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), constantConfig.getInternalError(), ex.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RollbackException.class)
    public ResponseEntity<Object> rollbackException(RollbackException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), constantConfig.getInternalError(), ex.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Object> dataIntegrityViolationException(DataIntegrityViolationException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), constantConfig.getInternalError(), ex.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> validationExceptionException(ValidationException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), constantConfig.getInternalError(), ex.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> entityNotFoundException(EntityNotFoundException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), constantConfig.getInternalError(), ex.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DateTimeException.class)
    public ResponseEntity<Object> DateTimeException(DateTimeException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), constantConfig.getInternalError(), ex.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> notFoundException(NotFoundException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.NOT_FOUND.value(), constantConfig.getNotFound(), ex.getMessage(), request.getRequestURI()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(GenericJDBCException.class)
    public ResponseEntity<Object> genericJDBCException(GenericJDBCException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.NOT_FOUND.value(), constantConfig.getNotFound(), ex.getMessage(), request.getRequestURI()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<Object> forbiddenException(ForbiddenException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.FORBIDDEN.value(), constantConfig.getForbidden(), ex.getMessage(), request.getRequestURI()), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<Object> unauthorizedException(UnauthorizedException ex, HttpServletRequest request) {
        return new ResponseEntity<>(new SimpleObjectMessage(HttpStatus.UNAUTHORIZED.value(), constantConfig.getUnauthorized(), ex.getMessage(), request.getRequestURI()), HttpStatus.UNAUTHORIZED);
    }
}