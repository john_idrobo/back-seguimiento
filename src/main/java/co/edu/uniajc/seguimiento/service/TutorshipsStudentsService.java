package co.edu.uniajc.seguimiento.service;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.TutorshipsStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.AttendanceDto;
import co.edu.uniajc.seguimiento.model.dto.ScheduleDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsStudentsDto;
import co.edu.uniajc.seguimiento.model.transform.AttendanceTransform;
import co.edu.uniajc.seguimiento.model.transform.ScheduleTransform;
import co.edu.uniajc.seguimiento.model.transform.TutorshipsStudentsTransform;
import co.edu.uniajc.seguimiento.model.transform.TutorshipsTransform;
import co.edu.uniajc.seguimiento.repository.AttendanceRepository;
import co.edu.uniajc.seguimiento.repository.ScheduleRepository;
import co.edu.uniajc.seguimiento.repository.TutorshipsRepository;
import co.edu.uniajc.seguimiento.repository.TutorshipsStudentsRepository;
import co.edu.uniajc.seguimiento.utility.exception.BadRequestException;
import co.edu.uniajc.seguimiento.utility.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TutorshipsStudentsService {

    private final TutorshipsStudentsRepository tutorshipsStudentsRepository;
    private final TutorshipsRepository tutorshipsRepository;
    private final ScheduleRepository scheduleRepository;
    private final AttendanceRepository attendanceRepository;
    private final TutorshipsStudentsTransform tutorshipsStudentsTransform;
    private final TutorshipsTransform tutorshipsTransform;
    private final ScheduleTransform scheduleTransform;
    private final AttendanceTransform attendanceTransform;
    private final List<Object[]> list;
    private final ConstantConfig constantConfig;

    @Autowired
    public TutorshipsStudentsService(TutorshipsStudentsRepository tutorshipsStudentsRepository, TutorshipsRepository tutorshipsRepository, ScheduleRepository scheduleRepository, AttendanceRepository attendanceRepository, TutorshipsStudentsTransform tutorshipsStudentsTransform, TutorshipsTransform tutorshipsTransform, ScheduleTransform scheduleTransform, AttendanceTransform attendanceTransform, List<Object[]> list, ConstantConfig constantConfig) {
        this.tutorshipsStudentsRepository = tutorshipsStudentsRepository;
        this.tutorshipsRepository = tutorshipsRepository;
        this.scheduleRepository = scheduleRepository;
        this.attendanceRepository = attendanceRepository;
        this.tutorshipsStudentsTransform = tutorshipsStudentsTransform;
        this.tutorshipsTransform = tutorshipsTransform;
        this.scheduleTransform = scheduleTransform;
        this.attendanceTransform = attendanceTransform;
        this.list = list;
        this.constantConfig = constantConfig;
    }

    public List<TutorshipsStudentsDto> findAllTutorshipsStudentsByFinished(Boolean finished) {
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinished(finished).stream()
                .map(tutorshipsStudentsTransform::tutorshipsStudentsModelToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (tutorshipsStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotStudents());
        }
        return tutorshipsStudentsDto;
    }

    public List<TutorshipsStudentsDto> findAllTutorshipsStudentsByFinishedAndStudents(Boolean finished, String students) {
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinishedAndStudents(finished, students).stream()
                .map(tutorshipsStudentsTransform::tutorshipsStudentsModelToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (tutorshipsStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotStudents());
        }
        return tutorshipsStudentsDto;
    }

    public List<TutorshipsStudentsDto> findAllTutorshipsStudentsByFinishedAndTutorshipsAndUsers(Boolean finished, Integer tutorships, String users) {
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinishedAndTutorshipsAndUsers(finished, tutorships, users).stream()
                .map(tutorshipsStudentsTransform::tutorshipsStudentsModelToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (tutorshipsStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotStudents());
        }
        return tutorshipsStudentsDto;
    }

    public List<TutorshipsStudentsDto> findAllTutorshipsStudentsByFinishedAndTutorshipsAndStudentsAndUsers(Boolean finished, Integer tutorships, String students, String users) {
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinishedAndTutorshipsAndStudentsAndUsers(finished, tutorships, students, users).stream()
                .map(tutorshipsStudentsTransform::tutorshipsStudentsModelToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (tutorshipsStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotStudents());
        }
        return tutorshipsStudentsDto;
    }

    public List<TutorshipsStudentsDto> findAllTutorshipsStudentsByFinishedAndSubjectsAndTutorshipsAndUsers(Boolean finished, String subjects, Integer tutorships, String users) {
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinishedAndSubjectsAndTutorshipsAndUsers(finished, subjects, tutorships, users).stream()
                .map(tutorshipsStudentsTransform::tutorshipsStudentsModelToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (tutorshipsStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotStudents());
        }
        return tutorshipsStudentsDto;
    }

    public List<TutorshipsStudentsDto> findAllTutorshipsStudentsByFinishedAndGroupAndTutorshipsAndUsers(Boolean finished, String group, Integer tutorships, String users) {
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.findAllTutorshipsStudentsByFinishedAndGroupAndTutorshipsAndUsers(finished, group, tutorships, users).stream()
                .map(tutorshipsStudentsTransform::tutorshipsStudentsModelToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (tutorshipsStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotStudents());
        }
        return tutorshipsStudentsDto;
    }

    public TutorshipsStudentsDto saveTutorshipsStudents(TutorshipsStudentsDto tutorshipsStudentsDto) {
        TutorshipsStudentsModel tutorshipsStudentsModel = tutorshipsStudentsTransform.tutorshipsStudentsDtoToTutorshipsStudentsModel(tutorshipsStudentsDto);
        int responseOne = tutorshipsRepository.existsTutorshipsByIdAndUsers(tutorshipsStudentsModel.getTutorships().getIdTutorships(), tutorshipsStudentsModel.getTutorships().getUsers());
        if (responseOne > 0) {
            int responseTwo = scheduleRepository.findScheduleByTutorshipsAndUsers(tutorshipsStudentsModel.getTutorships().getIdTutorships(), tutorshipsStudentsModel.getTutorships().getUsers());
            if (responseTwo > 0) {
                int responseThree = tutorshipsStudentsRepository.existsTutorshipsStudentsByTutorshipsAndStudents(tutorshipsStudentsModel.getTutorships().getIdTutorships(), tutorshipsStudentsModel.getStudents());
                if (responseThree == 0) {
                    int responseFour = tutorshipsStudentsRepository.existsTutorshipsStudentsByGroupAndSubjectsAndStudents(tutorshipsStudentsModel.getGroup(), tutorshipsStudentsModel.getSubjects(), tutorshipsStudentsModel.getStudents());
                    if (responseFour == 0) {
                        scheduleRepository.findScheduleByFinishedAndTutorshipsAndUsers(false, tutorshipsStudentsModel.getTutorships().getIdTutorships(), tutorshipsStudentsModel.getTutorships().getUsers()).forEach((p) ->
                                scheduleRepository.findScheduleByStudents(tutorshipsStudentsModel.getStudents()).forEach((r) -> {
                                    if (p.getWeekday().equals(r.getWeekday())) {
                                        String objectOne = scheduleRepository.existsScheduleByWeekdayAndStartAndEndAndStudents(p.getWeekday(), p.getStartTime(), p.getEndTime(), tutorshipsStudentsModel.getStudents()).stream()
                                                .map(Object::toString)
                                                .collect(Collectors.joining(" -- ", "{", "}"));
                                        if (!objectOne.equals("{}")) {
                                            throw new BadRequestException(constantConfig.getCrossingSchedule() + constantConfig.getFetch() + objectOne);
                                        }
                                    }
                                }));
                    } else {
                        throw new BadRequestException(constantConfig.getBadTutorshipsStudents());
                    }
                } else {
                    throw new BadRequestException(constantConfig.getBadStudents());
                }
            } else {
                throw new NotFoundException(constantConfig.getNotSchedule());
            }
        } else {
            throw new NotFoundException(constantConfig.getUsers());
        }
        return tutorshipsStudentsTransform.tutorshipsStudentsModelToTutorshipsStudentsDto(tutorshipsStudentsRepository.save(tutorshipsStudentsModel));
    }

    public TutorshipsStudentsDto updateTutorshipsStudents(TutorshipsStudentsDto tutorshipsStudentsDto) {
        TutorshipsStudentsModel tutorshipsStudentsModel = tutorshipsStudentsTransform.tutorshipsStudentsDtoToTutorshipsStudentsModel(tutorshipsStudentsDto);
        int response = tutorshipsStudentsRepository.existsTutorshipsStudentsByIdAndGroupAndSubjectsAndStudents(tutorshipsStudentsModel.getIdTutorshipsStudents(), tutorshipsStudentsModel.getGroup(), tutorshipsStudentsModel.getSubjects(), tutorshipsStudentsModel.getStudents());
        if (response == 0) {
            return tutorshipsStudentsTransform.tutorshipsStudentsModelToTutorshipsStudentsDto(tutorshipsStudentsRepository.save(tutorshipsStudentsModel));
        } else {
            throw new BadRequestException(constantConfig.getBadTutorshipsStudents());
        }
    }

    public String inactiveTutorshipsStudentsById(Integer id) {
        int response = tutorshipsStudentsRepository.inactiveTutorshipsStudentsById(id);
        if (response == 0) {
            throw new NotFoundException(constantConfig.getNotStudents());
        }
        return constantConfig.getInactiveStudents();
    }

    public List<Object[]> reportTutorshipsStudentsByTutorshipsAndStudentsAndUsers(Integer tutorships, String students, String users) {
        list.clear();
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.reportTutorshipsByFinishedAndIdAndUsers(false, tutorships, users).stream()
                .map(tutorshipsTransform::objectToTutorshipsDto)
                .collect(Collectors.toList());
        List<ScheduleDto> scheduleDto = scheduleRepository.findScheduleByFinishedAndTutorshipsAndUsers(false, tutorships, users).stream()
                .map(scheduleTransform::scheduleModelToScheduleDto)
                .collect(Collectors.toList());
        List<AttendanceDto> attendanceDto = attendanceRepository.findAllAttendanceByFinishedAndTutorshipsAndStudentsAndUsers(false, tutorships, students, users).stream()
                .map(attendanceTransform::attendanceModelToAttendanceDto)
                .collect(Collectors.toList());
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.reportTutorshipsStudentsByTutorshipsAndStudentsAndUsers(tutorships, students, users).stream()
                .map(tutorshipsStudentsTransform::objToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (!tutorshipsDto.isEmpty()) {
            list.add(0, tutorshipsDto.toArray());
            if (!scheduleDto.isEmpty()) {
                list.add(1, scheduleDto.toArray());
                if (!attendanceDto.isEmpty()) {
                    list.add(2, attendanceDto.toArray());
                    if (!tutorshipsStudentsDto.isEmpty()) {
                        list.add(3, tutorshipsStudentsDto.toArray());
                    } else {
                        throw new NotFoundException(constantConfig.getNotStudents());
                    }
                } else {
                    throw new NotFoundException(constantConfig.getNotAttendance());
                }
            } else {
                throw new NotFoundException(constantConfig.getNotSchedule());
            }
        } else {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return list;
    }

    public List<Object[]> reportAverageTutorshipsStudentsByTutorshipsAndStudentsAndUsers(Integer tutorships, String students, String users) {
        list.clear();
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.reportTutorshipsByFinishedAndIdAndUsers(true, tutorships, users).stream()
                .map(tutorshipsTransform::objectToTutorshipsDto)
                .collect(Collectors.toList());
        List<ScheduleDto> scheduleDto = scheduleRepository.findScheduleByFinishedAndTutorshipsAndUsers(true, tutorships, users).stream()
                .map(scheduleTransform::scheduleModelToScheduleDto)
                .collect(Collectors.toList());
        List<AttendanceDto> attendanceDto = attendanceRepository.findAllAttendanceByFinishedAndTutorshipsAndStudentsAndUsers(true, tutorships, students, users).stream()
                .map(attendanceTransform::attendanceModelToAttendanceDto)
                .collect(Collectors.toList());
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.reportAverageTutorshipsStudentsByTutorshipsAndStudentsAndUsers(tutorships, students, users).stream()
                .map(tutorshipsStudentsTransform::objAvgToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (!tutorshipsDto.isEmpty()) {
            list.add(0, tutorshipsDto.toArray());
            if (!scheduleDto.isEmpty()) {
                list.add(1, scheduleDto.toArray());
                if (!attendanceDto.isEmpty()) {
                    list.add(2, attendanceDto.toArray());
                    if (!tutorshipsStudentsDto.isEmpty()) {
                        list.add(3, tutorshipsStudentsDto.toArray());
                    } else {
                        throw new NotFoundException(constantConfig.getNotStudents());
                    }
                } else {
                    throw new NotFoundException(constantConfig.getNotAttendance());
                }
            } else {
                throw new NotFoundException(constantConfig.getNotSchedule());
            }
        } else {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return list;
    }

    public List<Object[]> reportTutorshipsStudentsByTutorshipsAndUsers(Integer tutorships, String users) {
        list.clear();
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.reportTutorshipsByFinishedAndIdAndUsers(false, tutorships, users).stream()
                .map(tutorshipsTransform::objectToTutorshipsDto)
                .collect(Collectors.toList());
        List<ScheduleDto> scheduleDto = scheduleRepository.findScheduleByFinishedAndTutorshipsAndUsers(false, tutorships, users).stream()
                .map(scheduleTransform::scheduleModelToScheduleDto)
                .collect(Collectors.toList());
        List<AttendanceDto> attendanceDto = attendanceRepository.findAllAttendanceByFinishedAndTutorshipsAndUsers(false, tutorships, users).stream()
                .map(attendanceTransform::attendanceModelToAttendanceDto)
                .collect(Collectors.toList());
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.reportTutorshipsStudentsByTutorshipsAndUsers(tutorships, users).stream()
                .map(tutorshipsStudentsTransform::objToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (!tutorshipsDto.isEmpty()) {
            list.add(0, tutorshipsDto.toArray());
            if (!scheduleDto.isEmpty()) {
                list.add(1, scheduleDto.toArray());
                if (!attendanceDto.isEmpty()) {
                    list.add(2, attendanceDto.toArray());
                    if (!tutorshipsStudentsDto.isEmpty()) {
                        list.add(3, tutorshipsStudentsDto.toArray());
                    } else {
                        throw new NotFoundException(constantConfig.getNotStudents());
                    }
                } else {
                    throw new NotFoundException(constantConfig.getNotAttendance());
                }
            } else {
                throw new NotFoundException(constantConfig.getNotSchedule());
            }
        } else {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return list;
    }

    public List<Object[]> reportAverageTutorshipsStudentsByTutorshipsAndUsers(Integer tutorships, String users) {
        list.clear();
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.reportTutorshipsByFinishedAndIdAndUsers(true, tutorships, users).stream()
                .map(tutorshipsTransform::objectToTutorshipsDto)
                .collect(Collectors.toList());
        List<ScheduleDto> scheduleDto = scheduleRepository.findScheduleByFinishedAndTutorshipsAndUsers(true, tutorships, users).stream()
                .map(scheduleTransform::scheduleModelToScheduleDto)
                .collect(Collectors.toList());
        List<AttendanceDto> attendanceDto = attendanceRepository.findAllAttendanceByFinishedAndTutorshipsAndUsers(true, tutorships, users).stream()
                .map(attendanceTransform::attendanceModelToAttendanceDto)
                .collect(Collectors.toList());
        List<TutorshipsStudentsDto> tutorshipsStudentsDto = tutorshipsStudentsRepository.reportAverageTutorshipsStudentsByTutorshipsAndUsers(tutorships, users).stream()
                .map(tutorshipsStudentsTransform::objAvgToTutorshipsStudentsDto)
                .collect(Collectors.toList());
        if (!tutorshipsDto.isEmpty()) {
            list.add(0, tutorshipsDto.toArray());
            if (!scheduleDto.isEmpty()) {
                list.add(1, scheduleDto.toArray());
                if (!attendanceDto.isEmpty()) {
                    list.add(2, attendanceDto.toArray());
                    if (!tutorshipsStudentsDto.isEmpty()) {
                        list.add(3, tutorshipsStudentsDto.toArray());
                    } else {
                        throw new NotFoundException(constantConfig.getNotStudents());
                    }
                } else {
                    throw new NotFoundException(constantConfig.getNotAttendance());
                }
            } else {
                throw new NotFoundException(constantConfig.getNotSchedule());
            }
        } else {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return list;
    }
}