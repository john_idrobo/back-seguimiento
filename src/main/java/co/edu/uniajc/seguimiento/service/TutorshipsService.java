package co.edu.uniajc.seguimiento.service;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import co.edu.uniajc.seguimiento.model.transform.TutorshipsTransform;
import co.edu.uniajc.seguimiento.repository.TutorshipsRepository;
import co.edu.uniajc.seguimiento.utility.exception.BadRequestException;
import co.edu.uniajc.seguimiento.utility.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TutorshipsService {

    private final TutorshipsRepository tutorshipsRepository;
    private final TutorshipsTransform tutorshipsTransform;
    private final ConstantConfig constantConfig;

    @Autowired
    public TutorshipsService(TutorshipsRepository tutorshipsRepository, TutorshipsTransform tutorshipsTransform, ConstantConfig constantConfig) {
        this.tutorshipsRepository = tutorshipsRepository;
        this.tutorshipsTransform = tutorshipsTransform;
        this.constantConfig = constantConfig;
    }

    public List<TutorshipsDto> findAllTutorshipsByFinished(Boolean finished) {
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.findAllTutorshipsByFinished(finished).stream()
                .map(tutorshipsTransform::tutorshipsModelToTutorshipsDto)
                .collect(Collectors.toList());
        if (tutorshipsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return tutorshipsDto;
    }

    public List<TutorshipsDto> findTutorshipsByFinishedAndId(Boolean finished, Integer id) {
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.findTutorshipsByFinishedAndId(finished, id).stream()
                .map(tutorshipsTransform::tutorshipsModelToTutorshipsDto)
                .collect(Collectors.toList());
        if (tutorshipsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return tutorshipsDto;
    }

    public List<TutorshipsDto> findAllTutorshipsByFinishedAndUsers(Boolean finished, String users) {
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.findAllTutorshipsByFinishedAndUsers(finished, users).stream()
                .map(tutorshipsTransform::tutorshipsModelToTutorshipsDto)
                .collect(Collectors.toList());
        if (tutorshipsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return tutorshipsDto;
    }

    public List<TutorshipsDto> findAllTutorshipsByFinishedAndDate(Boolean finished, LocalDate date) {
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.findAllTutorshipsByFinishedAndDate(finished, date).stream()
                .map(tutorshipsTransform::tutorshipsModelToTutorshipsDto)
                .collect(Collectors.toList());
        if (tutorshipsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return tutorshipsDto;
    }

    public List<TutorshipsDto> findAllTutorshipsByFinishedAndZone(Boolean finished, String zone) {
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.findAllTutorshipsByFinishedAndZone(finished, zone).stream()
                .map(tutorshipsTransform::tutorshipsModelToTutorshipsDto)
                .collect(Collectors.toList());
        if (tutorshipsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return tutorshipsDto;
    }

    public List<TutorshipsDto> findAllTutorshipsByFinishedAndDateAndUsers(Boolean finished, LocalDate date, String users) {
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.findAllTutorshipsByFinishedAndDateAndUsers(finished, date, users).stream()
                .map(tutorshipsTransform::tutorshipsModelToTutorshipsDto)
                .collect(Collectors.toList());
        if (tutorshipsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return tutorshipsDto;
    }

    public List<TutorshipsDto> findAllTutorshipsByFinishedAndZoneAndUsers(Boolean finished, String zone, String users) {
        List<TutorshipsDto> tutorshipsDto = tutorshipsRepository.findAllTutorshipsByFinishedAndZoneAndUsers(finished, zone, users).stream()
                .map(tutorshipsTransform::tutorshipsModelToTutorshipsDto)
                .collect(Collectors.toList());
        if (tutorshipsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return tutorshipsDto;
    }

    public TutorshipsDto saveTutorships(TutorshipsDto tutorshipsDto) {
        TutorshipsModel tutorshipsModel = tutorshipsTransform.tutorshipsDtoToTutorshipsModel(tutorshipsDto);
        int response = tutorshipsRepository.existsTutorshipsByZoneAndDescriptionAndUsers(tutorshipsModel.getTimeZone(), tutorshipsModel.getDescription(), tutorshipsModel.getUsers());
        if (response > 0) {
            throw new BadRequestException(constantConfig.getBadTutorships());
        }
        return tutorshipsTransform.tutorshipsModelToTutorshipsDto(tutorshipsRepository.save(tutorshipsModel));
    }

    public String inactiveTutorshipsById(Integer id) {
        int response = tutorshipsRepository.inactiveTutorshipsById(id);
        if (response == 0) {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return constantConfig.getInactiveTutorships();
    }

    public String finishedTutorshipsById(Integer id) {
        int response = tutorshipsRepository.finishedTutorshipsById(id);
        if (response == 0) {
            throw new NotFoundException(constantConfig.getNotTutorships());
        }
        return constantConfig.getFinishedTutorships();
    }
}