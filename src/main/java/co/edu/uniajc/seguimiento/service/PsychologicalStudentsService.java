package co.edu.uniajc.seguimiento.service;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.PsychologicalStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.PsychologicalStudentsDto;
import co.edu.uniajc.seguimiento.model.transform.PsychologicalStudentsTransform;
import co.edu.uniajc.seguimiento.repository.PsychologicalStudentsRepository;
import co.edu.uniajc.seguimiento.utility.exception.BadRequestException;
import co.edu.uniajc.seguimiento.utility.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PsychologicalStudentsService {

    private final PsychologicalStudentsRepository psychologicalStudentsRepository;
    private final PsychologicalStudentsTransform psychologicalStudentsTransform;
    private final ConstantConfig constantConfig;

    @Autowired
    public PsychologicalStudentsService(PsychologicalStudentsRepository psychologicalStudentsRepository, PsychologicalStudentsTransform psychologicalStudentsTransform, ConstantConfig constantConfig) {
        this.psychologicalStudentsRepository = psychologicalStudentsRepository;
        this.psychologicalStudentsTransform = psychologicalStudentsTransform;
        this.constantConfig = constantConfig;
    }

    public List<PsychologicalStudentsDto> findAllPsychologicalStudents() {
        List<PsychologicalStudentsDto> psychologicalStudentsDto = psychologicalStudentsRepository.findAllPsychologicalStudents().stream()
                .map(psychologicalStudentsTransform::objectToPsychologicalStudentsDto)
                .collect(Collectors.toList());
        if (psychologicalStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotPsychological());
        }
        return psychologicalStudentsDto;
    }

    public List<PsychologicalStudentsDto> findAllPsychologicalStudentsByUsers(String users) {
        List<PsychologicalStudentsDto> psychologicalStudentsDto = psychologicalStudentsRepository.findAllPsychologicalStudentsByUsers(users).stream()
                .map(psychologicalStudentsTransform::objectToPsychologicalStudentsDto)
                .collect(Collectors.toList());
        if (psychologicalStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotPsychological());
        }
        return psychologicalStudentsDto;
    }

    public List<PsychologicalStudentsDto> findAllPsychologicalStudentsByStudents(String students) {
        List<PsychologicalStudentsDto> psychologicalStudentsDto = psychologicalStudentsRepository.findAllPsychologicalStudentsByStudents(students).stream()
                .map(psychologicalStudentsTransform::objectToPsychologicalStudentsDto)
                .collect(Collectors.toList());
        if (psychologicalStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotPsychological());
        }
        return psychologicalStudentsDto;
    }

    public List<PsychologicalStudentsDto> findAllPsychologicalStudentsByDate(LocalDate date) {
        List<PsychologicalStudentsDto> psychologicalStudentsDto = psychologicalStudentsRepository.findAllPsychologicalStudentsByDate(date).stream()
                .map(psychologicalStudentsTransform::objectToPsychologicalStudentsDto)
                .collect(Collectors.toList());
        if (psychologicalStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotPsychological());
        }
        return psychologicalStudentsDto;
    }

    public List<PsychologicalStudentsDto> findAllPsychologicalStudentsByDateAndUsers(LocalDate date, String users) {
        List<PsychologicalStudentsDto> psychologicalStudentsDto = psychologicalStudentsRepository.findAllPsychologicalStudentsByDateAndUsers(date, users).stream()
                .map(psychologicalStudentsTransform::objectToPsychologicalStudentsDto)
                .collect(Collectors.toList());
        if (psychologicalStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotPsychological());
        }
        return psychologicalStudentsDto;
    }

    public List<PsychologicalStudentsDto> findAllPsychologicalStudentsByStudentsAndUsers(String students, String users) {
        List<PsychologicalStudentsDto> psychologicalStudentsDto = psychologicalStudentsRepository.findAllPsychologicalStudentsByStudentsAndUsers(students, users).stream()
                .map(psychologicalStudentsTransform::objectToPsychologicalStudentsDto)
                .collect(Collectors.toList());
        if (psychologicalStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotPsychological());
        }
        return psychologicalStudentsDto;
    }

    public PsychologicalStudentsDto savePsychologicalStudents(PsychologicalStudentsDto psychologicalStudentsDto) {
        PsychologicalStudentsModel psychologicalStudentsModel = psychologicalStudentsTransform.psychologicalStudentsDtoToPsychologicalStudentsModel(psychologicalStudentsDto);
        int response = psychologicalStudentsRepository.existsPsychologicalStudentsByStudentsAndUsers(psychologicalStudentsModel.getStudents(), psychologicalStudentsModel.getUsers());
        if (response > 0) {
            throw new BadRequestException(constantConfig.getBadPsychological());
        }
        return psychologicalStudentsTransform.psychologicalStudentsModelToPsychologicalStudentsDto(psychologicalStudentsRepository.save(psychologicalStudentsModel));
    }

    public PsychologicalStudentsDto updatePsychologicalStudents(PsychologicalStudentsDto psychologicalStudentsDto) {
        PsychologicalStudentsModel psychologicalStudentsModel = psychologicalStudentsTransform.psychologicalStudentsDtoToPsychologicalStudentsModel(psychologicalStudentsDto);
        return psychologicalStudentsTransform.psychologicalStudentsModelToPsychologicalStudentsDto(psychologicalStudentsRepository.save(psychologicalStudentsModel));
    }

    public String inactivePsychologicalStudentsById(Integer id) {
        int response = psychologicalStudentsRepository.inactivePsychologicalStudentsById(id);
        if (response == 0) {
            throw new NotFoundException(constantConfig.getNotPsychological());
        }
        return constantConfig.getInactivePsychological();
    }

    public List<PsychologicalStudentsDto> reportPsychologicalStudentsByIdAndStudentsAndUsers(Integer id, String students, String users) {
        List<PsychologicalStudentsDto> psychologicalStudentsDto = psychologicalStudentsRepository.reportPsychologicalStudentsByIdAndStudentsAndUsers(id, students, users).stream()
                .map(psychologicalStudentsTransform::objectToPsychologicalStudentsDto)
                .collect(Collectors.toList());
        if (psychologicalStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotPsychological());
        }
        return psychologicalStudentsDto;
    }
}