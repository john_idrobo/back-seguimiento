package co.edu.uniajc.seguimiento.service;


import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.ActivitiesStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.ActivitiesStudentsDto;
import co.edu.uniajc.seguimiento.model.transform.ActivitiesStudentsTransform;
import co.edu.uniajc.seguimiento.repository.ActivitiesStudentsRepository;
import co.edu.uniajc.seguimiento.utility.exception.BadRequestException;
import co.edu.uniajc.seguimiento.utility.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActivitiesStudentsService {

    private final ActivitiesStudentsRepository activitiesStudentsRepository;
    private final ActivitiesStudentsTransform activitiesStudentsTransform;
    private final ConstantConfig constantConfig;

    @Autowired
    public ActivitiesStudentsService(ActivitiesStudentsRepository activitiesStudentsRepository, ActivitiesStudentsTransform activitiesStudentsTransform, ConstantConfig constantConfig) {
        this.activitiesStudentsRepository = activitiesStudentsRepository;
        this.activitiesStudentsTransform = activitiesStudentsTransform;
        this.constantConfig = constantConfig;
    }

    public List<ActivitiesStudentsDto> findAllActivitiesStudents() {
        List<ActivitiesStudentsDto> activitiesStudentsDto = activitiesStudentsRepository.findAllActivitiesStudents().stream()
                .map(activitiesStudentsTransform::activitiesStudentsModelToActivitiesStudentsDto)
                .collect(Collectors.toList());
        if (activitiesStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotActivities());
        }
        return activitiesStudentsDto;
    }

    public List<ActivitiesStudentsDto> findAllActivitiesStudentsByDate(LocalDate date) {
        List<ActivitiesStudentsDto> activitiesStudentsDto = activitiesStudentsRepository.findAllActivitiesStudentsByDate(date).stream()
                .map(activitiesStudentsTransform::activitiesStudentsModelToActivitiesStudentsDto)
                .collect(Collectors.toList());
        if (activitiesStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotActivities());
        }
        return activitiesStudentsDto;
    }

    public List<ActivitiesStudentsDto> findAllActivitiesStudentsByActivities(Integer activities) {
        List<ActivitiesStudentsDto> activitiesStudentsDto = activitiesStudentsRepository.findAllActivitiesStudentsByActivities(activities).stream()
                .map(activitiesStudentsTransform::activitiesStudentsModelToActivitiesStudentsDto)
                .collect(Collectors.toList());
        if (activitiesStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotActivities());
        }
        return activitiesStudentsDto;
    }

    public List<ActivitiesStudentsDto> findAllActivitiesStudentsByStudents(String students) {
        List<ActivitiesStudentsDto> activitiesStudentsDto = activitiesStudentsRepository.findAllActivitiesStudentsByStudents(students).stream()
                .map(activitiesStudentsTransform::activitiesStudentsModelToActivitiesStudentsDto)
                .collect(Collectors.toList());
        if (activitiesStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotActivities());
        }
        return activitiesStudentsDto;
    }

    public ActivitiesStudentsDto saveActivitiesStudents(ActivitiesStudentsDto activitiesStudentsDto) {
        ActivitiesStudentsModel activitiesStudentsModel = activitiesStudentsTransform.activitiesStudentsDtoToActivitiesStudentsModel(activitiesStudentsDto);
        int response = activitiesStudentsRepository.existsActivitiesStudentsByDateAndActivitiesAndStudents(activitiesStudentsModel.getAttendanceDate(), activitiesStudentsModel.getActivities(), activitiesStudentsModel.getStudents());
        if (response > 0) {
            throw new BadRequestException(constantConfig.getBadActivities());
        }
        return activitiesStudentsTransform.activitiesStudentsModelToActivitiesStudentsDto(activitiesStudentsRepository.save(activitiesStudentsModel));
    }

    public ActivitiesStudentsDto updateActivitiesStudents(ActivitiesStudentsDto activitiesStudentsDto) {
        ActivitiesStudentsModel activitiesStudentsModel = activitiesStudentsTransform.activitiesStudentsDtoToActivitiesStudentsModel(activitiesStudentsDto);
        return activitiesStudentsTransform.activitiesStudentsModelToActivitiesStudentsDto(activitiesStudentsRepository.save(activitiesStudentsModel));
    }

    public String inactiveActivitiesStudentsById(Integer id) {
        int response = activitiesStudentsRepository.inactiveActivitiesStudentsById(id);
        if (response == 0) {
            throw new NotFoundException(constantConfig.getNotActivities());
        }
        return constantConfig.getInactiveActivities();
    }

    public List<ActivitiesStudentsDto> reportAllActivitiesStudents() {
        List<ActivitiesStudentsDto> activitiesStudentsDto = activitiesStudentsRepository.reportAllActivitiesStudents().stream()
                .map(activitiesStudentsTransform::objectToActivitiesStudentsDto)
                .collect(Collectors.toList());
        if (activitiesStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotActivities());
        }
        return activitiesStudentsDto;
    }

    public List<ActivitiesStudentsDto> reportAllActivitiesStudentsByStudents(String students) {
        List<ActivitiesStudentsDto> activitiesStudentsDto = activitiesStudentsRepository.reportAllActivitiesStudentsByStudents(students).stream()
                .map(activitiesStudentsTransform::objectToActivitiesStudentsDto)
                .collect(Collectors.toList());
        if (activitiesStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotActivities());
        }
        return activitiesStudentsDto;
    }

    public List<ActivitiesStudentsDto> reportAllActivitiesStudentsByActivities(Integer activities) {
        List<ActivitiesStudentsDto> activitiesStudentsDto = activitiesStudentsRepository.reportAllActivitiesStudentsByActivities(activities).stream()
                .map(activitiesStudentsTransform::objectToActivitiesStudentsDto)
                .collect(Collectors.toList());
        if (activitiesStudentsDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotActivities());
        }
        return activitiesStudentsDto;
    }
}