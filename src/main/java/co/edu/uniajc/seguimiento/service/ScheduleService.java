package co.edu.uniajc.seguimiento.service;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.ScheduleModel;
import co.edu.uniajc.seguimiento.model.dto.ScheduleDto;
import co.edu.uniajc.seguimiento.model.transform.ScheduleTransform;
import co.edu.uniajc.seguimiento.repository.ScheduleRepository;
import co.edu.uniajc.seguimiento.repository.TutorshipsRepository;
import co.edu.uniajc.seguimiento.utility.exception.BadRequestException;
import co.edu.uniajc.seguimiento.utility.exception.NotFoundException;
import co.edu.uniajc.seguimiento.utility.exception.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScheduleService {

    private final ScheduleRepository scheduleRepository;
    private final TutorshipsRepository tutorshipsRepository;
    private final ScheduleTransform scheduleTransform;
    private final ConstantConfig constantConfig;

    @Autowired
    public ScheduleService(ScheduleRepository scheduleRepository, TutorshipsRepository tutorshipsRepository, ScheduleTransform scheduleTransform, ConstantConfig constantConfig) {
        this.scheduleRepository = scheduleRepository;
        this.tutorshipsRepository = tutorshipsRepository;
        this.scheduleTransform = scheduleTransform;
        this.constantConfig = constantConfig;
    }

    public List<ScheduleDto> findScheduleByTutorshipsAndUsers(Integer tutorships, String users) {
        List<ScheduleDto> scheduleDto = scheduleRepository.findScheduleByFinishedAndTutorshipsAndUsers(false, tutorships, users).stream()
                .map(scheduleTransform::scheduleModelToScheduleDto)
                .collect(Collectors.toList());
        if (scheduleDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotSchedule());
        }
        return scheduleDto;
    }

    public ScheduleDto saveSchedule(ScheduleDto scheduleDto) {
        ScheduleModel scheduleModel = scheduleTransform.scheduleDtoToScheduleModel(scheduleDto);
        if (tutorshipsRepository.existsTutorshipsByIdAndUsers(scheduleModel.getTutorships().getIdTutorships(), scheduleModel.getTutorships().getUsers()) == 0) {
            throw new UnauthorizedException(constantConfig.getUsers());
        } else if (scheduleModel.getStartTime().getHour() < 7 || scheduleModel.getEndTime().getHour() >= 22) {
            throw new BadRequestException(constantConfig.getLimit());
        } else if (scheduleModel.getStartTime().isAfter(scheduleModel.getEndTime())) {
            throw new BadRequestException(constantConfig.getBefore());
        } else if (scheduleModel.getStartTime().compareTo(scheduleModel.getEndTime()) == 0 || scheduleModel.getStartTime().getHour() == scheduleModel.getEndTime().getHour() && scheduleModel.getEndTime().getMinute() - scheduleModel.getStartTime().getMinute() < 30) {
            throw new BadRequestException(constantConfig.getDuration());
        } else {
            if (scheduleModel.getIdSchedule() == null) {
                scheduleModel.setIdSchedule(0);
            }
            String objectOne = scheduleRepository.existsScheduleByIdAndWeekdayAndStartAndEndAndUsers(scheduleModel.getIdSchedule(), scheduleModel.getWeekday(), scheduleModel.getStartTime().minusMinutes(20), scheduleModel.getEndTime().plusMinutes(20), scheduleModel.getTutorships().getUsers()).stream()
                    .map(Object::toString)
                    .collect(Collectors.joining(" -- ", "{", "}"));
            if (!objectOne.equals("{}")) {
                throw new BadRequestException(constantConfig.getBadSchedule() + constantConfig.getFetch() + objectOne);
            } else {
                String objectTwo = scheduleRepository.existsScheduleByWeekdayAndStartAndEndAndTutorships(scheduleModel.getWeekday(), scheduleModel.getStartTime().minusMinutes(20), scheduleModel.getEndTime().plusMinutes(20), scheduleModel.getTutorships().getIdTutorships()).stream()
                        .map(Object::toString)
                        .collect(Collectors.joining(" -- ", "{", "}"));
                if (!objectTwo.equals("{}")) {
                    throw new BadRequestException(constantConfig.getCrossingSchedule() + constantConfig.getFetch() + objectTwo);
                }
            }
        }
        return scheduleTransform.scheduleModelToScheduleDto(scheduleRepository.save(scheduleModel));
    }

    public String inactiveScheduleById(Integer id) {
        int response = scheduleRepository.inactiveScheduleById(id);
        if (response == 0) {
            throw new NotFoundException(constantConfig.getNotSchedule());
        }
        return constantConfig.getInactiveSchedule();
    }
}