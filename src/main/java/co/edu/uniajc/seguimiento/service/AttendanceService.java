package co.edu.uniajc.seguimiento.service;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.config.DayOfWeekConfig;
import co.edu.uniajc.seguimiento.model.AttendanceModel;
import co.edu.uniajc.seguimiento.model.ScheduleModel;
import co.edu.uniajc.seguimiento.model.dto.AttendanceDto;
import co.edu.uniajc.seguimiento.model.transform.AttendanceTransform;
import co.edu.uniajc.seguimiento.repository.AttendanceRepository;
import co.edu.uniajc.seguimiento.repository.ScheduleRepository;
import co.edu.uniajc.seguimiento.utility.exception.BadRequestException;
import co.edu.uniajc.seguimiento.utility.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AttendanceService {
    private final AttendanceRepository attendanceRepository;
    private final ScheduleRepository scheduleRepository;
    private final AttendanceTransform attendanceTransform;
    private final ConstantConfig constantConfig;
    private final DayOfWeekConfig dayOfWeekConfig;

    @Autowired
    public AttendanceService(AttendanceRepository attendanceRepository, ScheduleRepository scheduleRepository, AttendanceTransform attendanceTransform, ConstantConfig constantConfig, DayOfWeekConfig dayOfWeekConfig) {
        this.attendanceRepository = attendanceRepository;
        this.scheduleRepository = scheduleRepository;
        this.attendanceTransform = attendanceTransform;
        this.constantConfig = constantConfig;
        this.dayOfWeekConfig = dayOfWeekConfig;
    }

    public List<AttendanceDto> findAllAttendanceByTutorshipsAndUsers(Integer tutorships, String users) {
        List<AttendanceDto> attendanceDto = attendanceRepository.findAllAttendanceByFinishedAndTutorshipsAndUsers(false, tutorships, users).stream()
                .map(attendanceTransform::attendanceModelToAttendanceDto)
                .collect(Collectors.toList());
        if (attendanceDto.isEmpty()) {
            throw new NotFoundException(constantConfig.getNotAttendance());
        }
        return attendanceDto;
    }

    public AttendanceDto saveAttendance(AttendanceDto attendanceDto) {
        AttendanceModel attendanceModel = attendanceTransform.attendanceDtoToAttendanceModel(attendanceDto);
        /*PARA ACTUALIZAR*/
        if (attendanceModel.getIdAttendance() == null) {
            attendanceModel.setIdAttendance(0);
        }
        /*BUSCA QUE LOS DATOS DE TUTORSHIPS_STUDENTS ENVIADOS EXISTAN*/
        int responseOne = scheduleRepository.existsScheduleBytutorshipsStudentsAndStudents(attendanceModel.getTutorshipsStudents().getIdTutorshipsStudents(), attendanceModel.getTutorshipsStudents().getStudents());
        if (responseOne > 0) {
            LocalTime startTimeOne = LocalTime.parse(attendanceModel.getAttendanceDateTime().format(DateTimeFormatter.ofPattern("HH:mm")));
            String dayOne = dayOfWeekConfig.convertDayOfWeek(attendanceModel.getAttendanceDateTime().getDayOfWeek().toString());
            /*BUSCA QUE LOS DATOS DE ATTENDANCE ENVIADOS COINCIDEN CON EL HORARIO*/
            ScheduleModel responseTwo = scheduleRepository.existsScheduleByStartAndWeekdayAndtutorshipsStudentsAndStudents(startTimeOne, dayOne, attendanceModel.getTutorshipsStudents().getIdTutorshipsStudents(), attendanceModel.getTutorshipsStudents().getStudents());
            if (responseTwo != null) {
                LocalDate dateOne = LocalDate.parse(attendanceModel.getAttendanceDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                LocalDateTime startDateTime = dateOne.atStartOfDay();
                LocalDateTime endDateTime = dateOne.atTime(23, 59);
                /*BUSCAR TODAS LAS ASISTENCIAS DEL DIA*/
                attendanceRepository.existsAttendanceAllByIdAndStartDateTimeAndEndDateTimeAndtutorshipsStudentsAndStudents(attendanceModel.getIdAttendance(), startDateTime, endDateTime, attendanceModel.getTutorshipsStudents().getIdTutorshipsStudents(), attendanceModel.getTutorshipsStudents().getStudents()).forEach((p) -> {
                    LocalTime startTimeTwo = LocalTime.parse(p.getAttendanceDateTime().format(DateTimeFormatter.ofPattern("HH:mm")));
                    String dayTwo = dayOfWeekConfig.convertDayOfWeek(p.getAttendanceDateTime().getDayOfWeek().toString());
                    /* BUSCA QUE LOS DATOS DE ATTENDANCE REGISTRADOS CONCIDAN CON EL HORARIO*/
                    ScheduleModel responseThree = scheduleRepository.existsScheduleByStartAndWeekdayAndtutorshipsStudentsAndStudents(startTimeTwo, dayTwo, attendanceModel.getTutorshipsStudents().getIdTutorshipsStudents(), attendanceModel.getTutorshipsStudents().getStudents());
                    if (responseThree.getIdSchedule().equals(responseTwo.getIdSchedule())) {
                        throw new BadRequestException(constantConfig.getLimitAttendance());
                    }
                });
            } else {
                throw new BadRequestException(constantConfig.getBadAttendance());
            }
        } else {
            throw new NotFoundException(constantConfig.getNotStudents());
        }
        return attendanceTransform.attendanceModelToAttendanceDto(attendanceRepository.save(attendanceModel));
    }

    public String inactiveAttendanceById(Integer id) {
        int response = attendanceRepository.inactiveAttendanceById(id);
        if (response == 0) {
            throw new NotFoundException(constantConfig.getNotAttendance());
        }
        return constantConfig.getInactiveAttendance();
    }
}