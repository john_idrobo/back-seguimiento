package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.dto.ScheduleDto;
import co.edu.uniajc.seguimiento.service.ScheduleService;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectMessage;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RequestMapping("/schedule")
@Api("This API Has A CRUD For Schedule")
public class ScheduleController {

    private final ScheduleService scheduleService;
    private final ConstantConfig constantConfig;

    @Autowired
    public ScheduleController(ScheduleService scheduleService, ConstantConfig constantConfig) {
        this.scheduleService = scheduleService;
        this.constantConfig = constantConfig;
    }

    @GetMapping(path = "/all/tutorships-users")
    @ApiOperation(value = "Find All Schedule By Id Tutorships And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllScheduleByTutorshipsAndUsers(@RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), scheduleService.findScheduleByTutorshipsAndUsers(tutorships,users)));
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save Schedule")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Creado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> saveSchedule(@RequestBody ScheduleDto scheduleDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreated(), scheduleService.saveSchedule(scheduleDto)));
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Schedule")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Actualizado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> updateSchedule(@RequestBody ScheduleDto scheduleDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedUpdate(), scheduleService.saveSchedule(scheduleDto)));
    }

    @PutMapping(path = "/inactive/id")
    @ApiOperation(value = "Inactive Schedule By Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Inactivado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> inactiveScheduleById(@RequestParam(name = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedInactive(), scheduleService.inactiveScheduleById(id)));
    }
}