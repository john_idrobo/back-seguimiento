package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsStudentsDto;
import co.edu.uniajc.seguimiento.service.TutorshipsStudentsService;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectMessage;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RequestMapping("/tutorships/students")
@Api("This API Has A CRUD For Tutorships Students")
public class TutorshipsStudentsController {

    private final TutorshipsStudentsService tutorshipsStudentsService;
    private final ConstantConfig constantConfig;

    @Autowired
    public TutorshipsStudentsController(TutorshipsStudentsService tutorshipsStudentsService, ConstantConfig constantConfig) {
        this.tutorshipsStudentsService = tutorshipsStudentsService;
        this.constantConfig = constantConfig;
    }

    @GetMapping(path = "/all/finished")
    @ApiOperation(value = "Find All Tutorships Students By Finished")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsStudentsByFinished(@RequestParam(name = "finished") Boolean finished) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.findAllTutorshipsStudentsByFinished(finished)));
    }

    @GetMapping(path = "/all/finished-students")
    @ApiOperation(value = "Find All Tutorships Students By Finished And Students")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsStudentsByFinishedAndStudents(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "students") String students) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.findAllTutorshipsStudentsByFinishedAndStudents(finished, students)));
    }

    @GetMapping(path = "/all/finished-tutorships-users")
    @ApiOperation(value = "Find All Tutorships Students By Finished And Tutorships And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsStudentsByFinishedAndTutorshipsAndUsers(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.findAllTutorshipsStudentsByFinishedAndTutorshipsAndUsers(finished, tutorships, users)));
    }

    @GetMapping(path = "/all/finished-tutorships-students-users")
    @ApiOperation(value = "Find All Tutorships Students By Finished And Tutorships And Students And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsStudentsByFinishedAndTutorshipsAndStudentsAndUsers(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "students") String students, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.findAllTutorshipsStudentsByFinishedAndTutorshipsAndStudentsAndUsers(finished, tutorships, students, users)));
    }

    @GetMapping(path = "/all/finished-subjects-tutorships-users")
    @ApiOperation(value = "Find All Tutorships Students By Finished And Subjects And Tutorships And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsStudentsByFinishedAndSubjectsAndTutorshipsAndUsers(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "subjects") String subjects, @RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.findAllTutorshipsStudentsByFinishedAndSubjectsAndTutorshipsAndUsers(finished, subjects, tutorships, users)));
    }

    @GetMapping(path = "/all/finished-group-tutorships-users")
    @ApiOperation(value = "Find All Tutorships Students By Finished And Group And Tutorships And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsStudentsByFinishedAndGroupAndTutorshipsAndUsers(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "group") String group, @RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.findAllTutorshipsStudentsByFinishedAndGroupAndTutorshipsAndUsers(finished, group, tutorships, users)));
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save Tutorships Students")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Creado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> saveTutorshipsStudents(@RequestBody TutorshipsStudentsDto tutorshipsStudentsDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreated(), tutorshipsStudentsService.saveTutorshipsStudents(tutorshipsStudentsDto)));
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Tutorships Students")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Actualizado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> updateTutorshipsStudents(@RequestBody TutorshipsStudentsDto tutorshipsStudentsDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedUpdate(), tutorshipsStudentsService.updateTutorshipsStudents(tutorshipsStudentsDto)));
    }

    @PutMapping(path = "/inactive/id")
    @ApiOperation(value = "Inactive Tutorships Students By Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Inactivado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> inactiveTutorshipsStudentsById(@RequestParam(name = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedInactive(), tutorshipsStudentsService.inactiveTutorshipsStudentsById(id)));
    }

    @GetMapping(path = "/report/tutorships-users")
    @ApiOperation(value = "Report Tutorships Students By Tutorships And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> reportTutorshipsStudentsByTutorshipsAndUsers(@RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.reportTutorshipsStudentsByTutorshipsAndUsers(tutorships, users)));
    }

    @GetMapping(path = "/report/average/tutorships-users")
    @ApiOperation(value = "Report Average Tutorships Students By Tutorships And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> reportAverageTutorshipsStudentsByTutorshipsAndUsers(@RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.reportAverageTutorshipsStudentsByTutorshipsAndUsers(tutorships, users)));
    }

    @GetMapping(path = "/report/tutorships-students-users")
    @ApiOperation(value = "Report Tutorships Students By Tutorships And Students And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> reportTutorshipsStudentsByTutorshipsAndStudentsAndUsers(@RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "students") String students, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.reportTutorshipsStudentsByTutorshipsAndStudentsAndUsers(tutorships, students, users)));
    }

    @GetMapping(path = "/report/average/tutorships-students-users")
    @ApiOperation(value = "Report Average Tutorships Students By Tutorships And Students And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> reportAverageTutorshipsStudentsByTutorshipsAndStudentsAndUsers(@RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "students") String students, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsStudentsService.reportAverageTutorshipsStudentsByTutorshipsAndStudentsAndUsers(tutorships, students, users)));
    }
}