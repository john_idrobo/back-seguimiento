package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.dto.PsychologicalStudentsDto;
import co.edu.uniajc.seguimiento.service.PsychologicalStudentsService;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectMessage;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RequestMapping("/psychological/students")
@Api("This API Has A CRUD For Psychological Students")
public class PsychologicalStudentsController {

    private final PsychologicalStudentsService psychologicalStudentsService;
    private final ConstantConfig constantConfig;

    @Autowired
    public PsychologicalStudentsController(PsychologicalStudentsService psychologicalStudentsService, ConstantConfig constantConfig) {
        this.psychologicalStudentsService = psychologicalStudentsService;
        this.constantConfig = constantConfig;
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Find All Psychological Students")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllPsychologicalStudents() {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), psychologicalStudentsService.findAllPsychologicalStudents()));
    }

    @GetMapping(path = "/all/users")
    @ApiOperation(value = "Find All Psychological Students By Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllPsychologicalStudentsByUsers(@RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), psychologicalStudentsService.findAllPsychologicalStudentsByUsers(users)));
    }

    @GetMapping(path = "/all/students")
    @ApiOperation(value = "Find All Psychological Students By Students")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllPsychologicalStudentsByStudents(@RequestParam(name = "students") String students) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), psychologicalStudentsService.findAllPsychologicalStudentsByStudents(students)));
    }

    @GetMapping(path = "/all/date")
    @ApiOperation(value = "Find All Psychological Students By Date")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllPsychologicalStudentsByDate(@RequestParam(name = "date") LocalDate date) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), psychologicalStudentsService.findAllPsychologicalStudentsByDate(date)));
    }

    @GetMapping(path = "/all/date-users")
    @ApiOperation(value = "Find All Psychological Students By Date And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllPsychologicalStudentsByDateAndUsers(@RequestParam(name = "date") LocalDate date, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), psychologicalStudentsService.findAllPsychologicalStudentsByDateAndUsers(date, users)));
    }

    @GetMapping(path = "/all/students-users")
    @ApiOperation(value = "Find All Psychological Students By Students And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllPsychologicalStudentsByStudentsAndUsers(@RequestParam(name = "students") String students, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), psychologicalStudentsService.findAllPsychologicalStudentsByStudentsAndUsers(students, users)));
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save Psychological Students")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Creado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> savePsychologicalStudents(@RequestBody PsychologicalStudentsDto psychologicalStudentsDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreated(), psychologicalStudentsService.savePsychologicalStudents(psychologicalStudentsDto)));
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Psychological Students")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Actualizado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> updatePsychologicalStudents(@RequestBody PsychologicalStudentsDto psychologicalStudentsDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedUpdate(), psychologicalStudentsService.updatePsychologicalStudents(psychologicalStudentsDto)));
    }

    @PutMapping(path = "/inactive/id")
    @ApiOperation(value = "Inactive Psychological Students By Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Inactivado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> inactivePsychologicalStudentsById(@RequestParam(name = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedInactive(), psychologicalStudentsService.inactivePsychologicalStudentsById(id)));
    }

    @GetMapping(path = "/report/id-students-users")
    @ApiOperation(value = "Report Psychological Students By Id And Students And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> reportPsychologicalStudentsByIdAndStudentsAndUsers(@RequestParam(name = "id") Integer id, @RequestParam(name = "students") String students, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), psychologicalStudentsService.reportPsychologicalStudentsByIdAndStudentsAndUsers(id, students, users)));
    }
}