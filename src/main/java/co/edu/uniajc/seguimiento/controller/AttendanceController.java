package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.dto.AttendanceDto;
import co.edu.uniajc.seguimiento.service.AttendanceService;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectMessage;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RequestMapping("/attendance")
@Api("This API Has A CRUD For Attendance")
public class AttendanceController {

    private final AttendanceService attendanceService;
    private final ConstantConfig constantConfig;

    @Autowired
    public AttendanceController(AttendanceService attendanceService, ConstantConfig constantConfig) {
        this.attendanceService = attendanceService;
        this.constantConfig = constantConfig;
    }

    @GetMapping(path = "/all/tutorships-users")
    @ApiOperation(value = "Find All Attendance By Id Tutorships And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllAttendanceByTutorshipsAndUsers(@RequestParam(name = "tutorships") Integer tutorships, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), attendanceService.findAllAttendanceByTutorshipsAndUsers(tutorships, users)));
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save Attendance")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Creado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> saveAttendance(@RequestBody AttendanceDto attendanceDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreated(), attendanceService.saveAttendance(attendanceDto)));
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Attendance")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Actualizado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> updateAttendance(@RequestBody AttendanceDto attendanceDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedUpdate(), attendanceService.saveAttendance(attendanceDto)));
    }

    @PutMapping(path = "/inactive/id")
    @ApiOperation(value = "Inactive Attendance By Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Inactivado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> inactiveAttendanceById(@RequestParam(name = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedInactive(), attendanceService.inactiveAttendanceById(id)));
    }
}