package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import co.edu.uniajc.seguimiento.service.TutorshipsService;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectMessage;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RequestMapping("/tutorships")
@Api("This API Has A CRUD For Tutorships")
public class TutorshipsController {

    private final TutorshipsService tutorshipsService;
    private final ConstantConfig constantConfig;

    @Autowired
    public TutorshipsController(TutorshipsService tutorshipsService, ConstantConfig constantConfig) {
        this.tutorshipsService = tutorshipsService;
        this.constantConfig = constantConfig;
    }

    @GetMapping(path = "/all/finished")
    @ApiOperation(value = "Find All Tutorships Finished")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsByFinished(@RequestParam(name = "finished") Boolean finished) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsService.findAllTutorshipsByFinished(finished)));
    }

    @GetMapping(path = "/finished-id")
    @ApiOperation(value = "Find Tutorships By Finished And Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findTutorshipsByFinishedAndId(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsService.findTutorshipsByFinishedAndId(finished, id)));
    }

    @GetMapping(path = "/all/finished-users")
    @ApiOperation(value = "Find All Tutorships By Finished And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsByFinishedAndUsers(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsService.findAllTutorshipsByFinishedAndUsers(finished, users)));
    }

    @GetMapping(path = "/all/finished-date")
    @ApiOperation(value = "Find All Tutorships By Finished And Date")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsByFinishedAndDate(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "date") LocalDate date) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsService.findAllTutorshipsByFinishedAndDate(finished, date)));
    }

    @GetMapping(path = "/all/finished-zone")
    @ApiOperation(value = "Find All Tutorships By Finished And Zone")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsByFinishedAndZone(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "zone") String zone) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsService.findAllTutorshipsByFinishedAndZone(finished, zone)));
    }

    @GetMapping(path = "/all/finished-date-users")
    @ApiOperation(value = "Find Tutorships By Finished And Date And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsByFinishedAndDateAndUsers(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "date") LocalDate date, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsService.findAllTutorshipsByFinishedAndDateAndUsers(finished, date, users)));
    }

    @GetMapping(path = "/all/finished-zone-users")
    @ApiOperation(value = "Find All Tutorships By Finished And Zone And Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllTutorshipsByFinishedAndZoneAndUsers(@RequestParam(name = "finished") Boolean finished, @RequestParam(name = "zone") String zone, @RequestParam(name = "users") String users) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), tutorshipsService.findAllTutorshipsByFinishedAndZoneAndUsers(finished, zone, users)));
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save Tutorships")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Creado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> saveTutorships(@RequestBody TutorshipsDto tutorshipsDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreated(), tutorshipsService.saveTutorships(tutorshipsDto)));
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Tutorships")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Actualizado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> updateTutorships(@RequestBody TutorshipsDto tutorshipsDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedUpdate(), tutorshipsService.saveTutorships(tutorshipsDto)));
    }

    @PutMapping(path = "/inactive/id")
    @ApiOperation(value = "Inactive Tutorships By Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Inactivado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> inactiveTutorshipsById(@RequestParam(name = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedInactive(), tutorshipsService.inactiveTutorshipsById(id)));
    }

    @PutMapping(path = "/finished/id")
    @ApiOperation(value = "Finished Tutorships By Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Finalizado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> finishedTutorshipsById(@RequestParam(name = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedFinished(), tutorshipsService.finishedTutorshipsById(id)));
    }
}