package co.edu.uniajc.seguimiento.controller;

import co.edu.uniajc.seguimiento.config.ConstantConfig;
import co.edu.uniajc.seguimiento.model.dto.ActivitiesStudentsDto;
import co.edu.uniajc.seguimiento.service.ActivitiesStudentsService;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectMessage;
import co.edu.uniajc.seguimiento.utility.response.SimpleObjectResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RequestMapping("/activities/students")
@Api("This API Has A CRUD For Activities Students")
public class ActivitiesStudentsController {

    private final ActivitiesStudentsService activitiesStudentsService;
    private final ConstantConfig constantConfig;

    @Autowired
    public ActivitiesStudentsController(ActivitiesStudentsService activitiesStudentsService, ConstantConfig constantConfig) {
        this.activitiesStudentsService = activitiesStudentsService;
        this.constantConfig = constantConfig;
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Find All Activities Students")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllActivitiesStudents() {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), activitiesStudentsService.findAllActivitiesStudents()));
    }

    @GetMapping(path = "/all/date")
    @ApiOperation(value = "Find All Activities Students By Date")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllActivitiesStudentsByDate(@RequestParam(name = "date") LocalDate date) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), activitiesStudentsService.findAllActivitiesStudentsByDate(date)));
    }

    @GetMapping(path = "/all/activities")
    @ApiOperation(value = "Find All Activities Students By Activities")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllActivitiesStudentsByActivities(@RequestParam(name = "activities") Integer activities) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), activitiesStudentsService.findAllActivitiesStudentsByActivities(activities)));
    }

    @GetMapping(path = "/all/students")
    @ApiOperation(value = "Find All Activities Students By Students")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> findAllActivitiesStudentsByStudents(@RequestParam(name = "students") String students) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), activitiesStudentsService.findAllActivitiesStudentsByStudents(students)));
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save Activities Students")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Creado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> saveActivitiesStudents(@RequestBody ActivitiesStudentsDto activitiesStudentsDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreated(), activitiesStudentsService.saveActivitiesStudents(activitiesStudentsDto)));
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Activities Students")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Actualizado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> updateActivitiesStudents(@RequestBody ActivitiesStudentsDto activitiesStudentsDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedUpdate(), activitiesStudentsService.updateActivitiesStudents(activitiesStudentsDto)));
    }

    @PutMapping(path = "/inactive/id")
    @ApiOperation(value = "Inactive Activities Students By Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso Inactivado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 400, message = "Sintaxis Inválida", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> inactiveActivitiesStudentsById(@RequestParam(name = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new SimpleObjectResponse(HttpStatus.CREATED.value(), constantConfig.getCreatedInactive(), activitiesStudentsService.inactiveActivitiesStudentsById(id)));
    }

    @GetMapping(path = "/report")
    @ApiOperation(value = "Report Activities Students")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> reportAllActivitiesStudents() {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), activitiesStudentsService.reportAllActivitiesStudents()));
    }

    @GetMapping(path = "/report/students")
    @ApiOperation(value = "Report Activities Students By Students")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> reportAllActivitiesStudentsByStudents(@RequestParam(name = "students") String students) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), activitiesStudentsService.reportAllActivitiesStudentsByStudents(students)));
    }

    @GetMapping(path = "/report/activities")
    @ApiOperation(value = "Report Activities Students By Activities")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Recurso Encontrado Exitosamente", response = SimpleObjectResponse.class),
            @ApiResponse(code = 404, message = "Recurso No Encontrado", response = SimpleObjectMessage.class),
            @ApiResponse(code = 500, message = "Error En Conexión DB", response = SimpleObjectMessage.class)})
    public ResponseEntity<Object> reportAllActivitiesStudentsByActivities(@RequestParam(name = "activities") Integer activities) {
        return ResponseEntity.status(HttpStatus.OK).body(new SimpleObjectResponse(HttpStatus.OK.value(), constantConfig.getOk(), activitiesStudentsService.reportAllActivitiesStudentsByActivities(activities)));
    }
}