package co.edu.uniajc.seguimiento.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleDto implements Serializable {

    @ApiModelProperty(value = "Id Horario", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idSchedule;

    @ApiModelProperty(value = "Día Del Horario", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String weekday;

    @ApiModelProperty(value = "Hora De Inicio Del Horario", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalTime startTime;

    @ApiModelProperty(value = "Hora De Fin Del Horario", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalTime endTime;

    @ApiModelProperty(value = "Dato Tutoría", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private TutorshipsDto tutorships;
}