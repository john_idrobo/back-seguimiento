package co.edu.uniajc.seguimiento.model.transform;

import co.edu.uniajc.seguimiento.model.PsychologicalStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.PsychologicalStudentsDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class PsychologicalStudentsTransform {

    public PsychologicalStudentsModel psychologicalStudentsDtoToPsychologicalStudentsModel(PsychologicalStudentsDto psychologicalStudentsDto) {
        return PsychologicalStudentsModel.builder()
                .idPsychologicalStudents(psychologicalStudentsDto.getIdPsychologicalStudents())
                .reason(psychologicalStudentsDto.getReason())
                .description(psychologicalStudentsDto.getDescription())
                .factors(psychologicalStudentsDto.getFactors())
                .familyBackground(psychologicalStudentsDto.getFamilyBackground())
                .diagnosis(psychologicalStudentsDto.getDiagnosis())
                .users(psychologicalStudentsDto.getUsers())
                .students(psychologicalStudentsDto.getStudents())
                .build();
    }

    public PsychologicalStudentsDto psychologicalStudentsModelToPsychologicalStudentsDto(PsychologicalStudentsModel psychologicalStudentsModel) {
        return PsychologicalStudentsDto.builder()
                .idPsychologicalStudents(psychologicalStudentsModel.getIdPsychologicalStudents())
                .reason(psychologicalStudentsModel.getReason())
                .description(psychologicalStudentsModel.getDescription())
                .factors(psychologicalStudentsModel.getFactors())
                .familyBackground(psychologicalStudentsModel.getFamilyBackground())
                .diagnosis(psychologicalStudentsModel.getDiagnosis())
                .users(psychologicalStudentsModel.getUsers())
                .students(psychologicalStudentsModel.getStudents())
                .build();
    }

    public PsychologicalStudentsDto objectToPsychologicalStudentsDto(Object[] object) {
        return PsychologicalStudentsDto.builder()
                .idPsychologicalStudents((Integer) object[0])
                .creationDate((LocalDate.parse(object[1].toString())))
                .reason((String) object[2])
                .description((String) object[3])
                .factors((String) object[4])
                .familyBackground((String) object[5])
                .diagnosis((String) object[6])
                .users((String) object[7])
                .students((String) object[8])
                .build();
    }
}