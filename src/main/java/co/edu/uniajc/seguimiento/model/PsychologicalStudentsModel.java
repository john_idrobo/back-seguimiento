package co.edu.uniajc.seguimiento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "psychological_students")
public class PsychologicalStudentsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ps_id")
    private Integer idPsychologicalStudents;

    @NotNull(message = "Campo Razón Obligatorio")
    @Length(min = 5, max = 500, message = "Campo Razón Inválido")
    @Column(name = "ps_reason")
    private String reason;

    @NotNull(message = "Campo Descripción Obligatorio")
    @Length(min = 5, max = 500, message = "Campo Descripción Inválido")
    @Column(name = "ps_description")
    private String description;

    @NotNull(message = "Campo Factores Obligatorio")
    @Length(min = 5, max = 500, message = "Campo Factores Inválido")
    @Column(name = "ps_factors")
    private String factors;

    @NotNull(message = "Campo Antecedentes Familiares Obligatorio")
    @Length(min = 5, max = 500, message = "Campo Antecedentes Familiares Inválido")
    @Column(name = "ps_family_background")
    private String familyBackground;

    @NotNull(message = "Campo Diagnóstico Obligatorio")
    @Length(min = 5, max = 500, message = "Campo Diagnóstico Inválido")
    @Column(name = "ps_diagnosis")
    private String diagnosis;

    @NotNull(message = "Campo Usuario Obligatorio")
    @Length(min = 6, max = 10, message = "Campo Usuario Inválido")
    @Column(name = "ps_users")
    private String users;

    @NotNull(message = "Campo Estudiante Obligatorio")
    @Length(min = 6, max = 10, message = "Campo Estudiante Inválido")
    @Column(name = "ps_students")
    private String students;
}