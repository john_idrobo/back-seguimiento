package co.edu.uniajc.seguimiento.model.transform;

import co.edu.uniajc.seguimiento.model.ActivitiesStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.ActivitiesStudentsDto;
import org.springframework.stereotype.Component;

@Component
public class ActivitiesStudentsTransform {

    public ActivitiesStudentsModel activitiesStudentsDtoToActivitiesStudentsModel(ActivitiesStudentsDto activitiesStudentsDto) {
        return ActivitiesStudentsModel.builder()
                .idActivitiesStudents(activitiesStudentsDto.getIdActivitiesStudents())
                .attendanceDate(activitiesStudentsDto.getAttendanceDate())
                .completedHours(activitiesStudentsDto.getCompletedHours())
                .activities(activitiesStudentsDto.getActivities())
                .students(activitiesStudentsDto.getStudents())
                .build();
    }

    public ActivitiesStudentsDto activitiesStudentsModelToActivitiesStudentsDto(ActivitiesStudentsModel activitiesStudentsModel) {
        return ActivitiesStudentsDto.builder()
                .idActivitiesStudents(activitiesStudentsModel.getIdActivitiesStudents())
                .attendanceDate(activitiesStudentsModel.getAttendanceDate())
                .completedHours(activitiesStudentsModel.getCompletedHours())
                .activities(activitiesStudentsModel.getActivities())
                .students(activitiesStudentsModel.getStudents())
                .build();
    }

    public ActivitiesStudentsDto objectToActivitiesStudentsDto(Object[] object) {
        return ActivitiesStudentsDto.builder()
                .activities((Integer) object[0])
                .students((String) object[1])
                .completedHours((Double) object[2])
                .build();
    }
}