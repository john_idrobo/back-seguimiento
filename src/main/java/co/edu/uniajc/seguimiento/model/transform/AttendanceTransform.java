package co.edu.uniajc.seguimiento.model.transform;

import co.edu.uniajc.seguimiento.model.AttendanceModel;
import co.edu.uniajc.seguimiento.model.TutorshipsStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.AttendanceDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsStudentsDto;
import org.springframework.stereotype.Component;

@Component
public class AttendanceTransform {

    public AttendanceModel attendanceDtoToAttendanceModel(AttendanceDto attendanceDto) {
        return AttendanceModel.builder()
                .idAttendance(attendanceDto.getIdAttendance())
                .attendanceDateTime(attendanceDto.getAttendanceDateTime())
                .attended(attendanceDto.getAttended())
                .tutorshipsStudents(TutorshipsStudentsModel.builder()
                        .idTutorshipsStudents(attendanceDto.getTutorshipsStudents().getIdTutorshipsStudents())
                        .students(attendanceDto.getTutorshipsStudents().getStudents())
                        .build())
                .build();
    }

    public AttendanceDto attendanceModelToAttendanceDto(AttendanceModel attendanceModel) {
        return AttendanceDto.builder()
                .idAttendance(attendanceModel.getIdAttendance())
                .attendanceDateTime(attendanceModel.getAttendanceDateTime())
                .attended(attendanceModel.getAttended())
                .tutorshipsStudents(TutorshipsStudentsDto.builder()
                        .idTutorshipsStudents(attendanceModel.getTutorshipsStudents().getIdTutorshipsStudents())
                        .students(attendanceModel.getTutorshipsStudents().getStudents())
                        .build())
                .build();
    }
}