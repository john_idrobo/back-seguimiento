package co.edu.uniajc.seguimiento.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceDto implements Serializable {

    @ApiModelProperty(value = "Id Asistenecia", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idAttendance;

    @ApiModelProperty(value = "Campo Fecha y Hora De Asistencia Obligatorio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime attendanceDateTime;

    @ApiModelProperty(value = "Asistió", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean attended;

    @ApiModelProperty(value = "Dato Tutoría Estudiante", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private TutorshipsStudentsDto tutorshipsStudents;
}