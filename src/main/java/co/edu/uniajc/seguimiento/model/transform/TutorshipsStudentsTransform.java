package co.edu.uniajc.seguimiento.model.transform;

import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.TutorshipsStudentsModel;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsStudentsDto;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class TutorshipsStudentsTransform {

    public TutorshipsStudentsModel tutorshipsStudentsDtoToTutorshipsStudentsModel(TutorshipsStudentsDto tutorshipsStudentsDto) {
        return TutorshipsStudentsModel.builder()
                .idTutorshipsStudents(tutorshipsStudentsDto.getIdTutorshipsStudents())
                .noteOne(tutorshipsStudentsDto.getNoteOne())
                .noteTwo(tutorshipsStudentsDto.getNoteTwo())
                .tutorships(TutorshipsModel.builder()
                        .idTutorships(tutorshipsStudentsDto.getTutorships().getIdTutorships())
                        .users(tutorshipsStudentsDto.getTutorships().getUsers())
                        .build())
                .subjects(tutorshipsStudentsDto.getSubjects())
                .group(tutorshipsStudentsDto.getGroup())
                .students(tutorshipsStudentsDto.getStudents())
                .build();
    }

    public TutorshipsStudentsDto tutorshipsStudentsModelToTutorshipsStudentsDto(TutorshipsStudentsModel tutorshipsStudentsModel) {
        return TutorshipsStudentsDto.builder()
                .idTutorshipsStudents(tutorshipsStudentsModel.getIdTutorshipsStudents())
                .noteOne(tutorshipsStudentsModel.getNoteOne())
                .noteTwo(tutorshipsStudentsModel.getNoteTwo())
                .tutorships(TutorshipsDto.builder()
                        .idTutorships(tutorshipsStudentsModel.getTutorships().getIdTutorships())
                        .users(tutorshipsStudentsModel.getTutorships().getUsers())
                        .build())
                .subjects(tutorshipsStudentsModel.getSubjects())
                .group(tutorshipsStudentsModel.getGroup())
                .students(tutorshipsStudentsModel.getStudents())
                .build();
    }

    public TutorshipsStudentsDto objToTutorshipsStudentsDto(Object[] object) {
        return TutorshipsStudentsDto.builder()
                .subjects((String) object[0])
                .group((String) object[1])
                .students((String) object[2])
                .noteOne((Double) object[3])
                .noteTwo((Double) object[4])
                .build();
    }

    public TutorshipsStudentsDto objAvgToTutorshipsStudentsDto(Object[] object) {
        return TutorshipsStudentsDto.builder()
                .subjects((String) object[0])
                .group((String) object[1])
                .students((String) object[2])
                .averageScore((BigDecimal) object[3])
                .build();
    }
}