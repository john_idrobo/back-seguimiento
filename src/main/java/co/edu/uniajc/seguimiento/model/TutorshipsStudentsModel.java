package co.edu.uniajc.seguimiento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tutorships_students")
public class TutorshipsStudentsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ts_id")
    private Integer idTutorshipsStudents;

    @NotNull(message = "Campo Nota Tutoría 1 Obligatorio")
    @Range(min = 0, max = 5, message = "Campo Nota Tutoría 1 Inválido")
    @Column(name = "ts_note_one")
    private Double noteOne;

    @NotNull(message = "Campo Nota Tutoría 2 Obligatorio")
    @Range(min = 0, max = 5, message = "Campo Nota Tutoría 2 Inválido")
    @Column(name = "ts_note_two")
    private Double noteTwo;

    @ManyToOne
    @JoinColumn(name = "ts_tutorships", referencedColumnName = "tu_id")
    private TutorshipsModel tutorships;

    @NotNull(message = "Campo Materia Obligatorio")
    @Length(min = 4, max = 10, message = "Campo Materia Inválido")
    @Column(name = "ts_subjects")
    private String subjects;

    @NotNull(message = "Campo Grupo Obligatorio")
    @Length(min = 4, max = 10, message = "Campo Grupo Inválido")
    @Column(name = "ts_group")
    private String group;

    @NotNull(message = "Campo Estudiante Obligatorio")
    @Length(min = 6, max = 10, message = "Campo Estudiante Inválido")
    @Column(name = "ts_students")
    private String students;
}