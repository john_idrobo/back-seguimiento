package co.edu.uniajc.seguimiento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "activities_students")
public class ActivitiesStudentsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "as_id")
    private Integer idActivitiesStudents;

    @NotNull(message = "Campo Fecha Asistencia Obligatorio")
    @PastOrPresent(message = "Campo Fecha Asistencia Inválido")
    @Column(name = "as_attendance_date")
    private LocalDate attendanceDate;

    @NotNull(message = "Campo Horas Completadas Obligatorio")
    @Range(min = 1, max = 10, message = "Campo Horas Completadas Inválido")
    @Column(name = "as_completed_hours")
    private Double completedHours;

    @NotNull(message = "Campo Actividad Obligatorio")
    @Range(min = 1, message = "Campo Actividad Inválido")
    @Column(name = "as_activities")
    private Integer activities;

    @NotNull(message = "Campo Estudiante Obligatorio")
    @Length(min = 6, max = 10, message = "Campo Estudiante Inválido")
    @Column(name = "as_students")
    private String students;
}