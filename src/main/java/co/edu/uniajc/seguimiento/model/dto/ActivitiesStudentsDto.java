package co.edu.uniajc.seguimiento.model.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivitiesStudentsDto implements Serializable {

    @ApiModelProperty(value = "Id Actividad Estudiante", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idActivitiesStudents;

    @ApiModelProperty(value = "Fecha Asistencia A La Actividad", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDate attendanceDate;

    @ApiModelProperty(value = "Horas Completadas En La Actividad", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double completedHours;

    @ApiModelProperty(value = "Datos Actividad", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer activities;

    @ApiModelProperty(value = "Datos Estudiante", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String students;
}