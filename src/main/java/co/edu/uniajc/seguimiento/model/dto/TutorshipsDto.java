package co.edu.uniajc.seguimiento.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TutorshipsDto implements Serializable {

    @ApiModelProperty(value = "Id Tutoría", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idTutorships;

    @ApiModelProperty(value = "Franja De La Tutoría", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String timeZone;

    @ApiModelProperty(value = "Descripción De La Tutoría", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String description;

    @ApiModelProperty(value = "Dato Usuario", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String users;

    @ApiModelProperty(value = "Fecha Creación De La Tutoría", required = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDate creationDate;
}