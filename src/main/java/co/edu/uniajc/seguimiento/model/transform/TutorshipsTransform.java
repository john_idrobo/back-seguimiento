package co.edu.uniajc.seguimiento.model.transform;

import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class TutorshipsTransform {

    public TutorshipsModel tutorshipsDtoToTutorshipsModel(TutorshipsDto tutorshipsDto) {
        return TutorshipsModel.builder()
                .idTutorships(tutorshipsDto.getIdTutorships())
                .timeZone(tutorshipsDto.getTimeZone())
                .description(tutorshipsDto.getDescription())
                .users(tutorshipsDto.getUsers())
                .build();
    }

    public TutorshipsDto tutorshipsModelToTutorshipsDto(TutorshipsModel tutorshipsModel) {
        return TutorshipsDto.builder()
                .idTutorships(tutorshipsModel.getIdTutorships())
                .timeZone(tutorshipsModel.getTimeZone())
                .description(tutorshipsModel.getDescription())
                .users(tutorshipsModel.getUsers())
                .build();
    }

    public TutorshipsDto objectToTutorshipsDto(Object[] object) {
        return TutorshipsDto.builder()
                .creationDate((LocalDate.parse(object[0].toString())))
                .timeZone((String) object[1])
                .description((String) object[2])
                .users((String) object[3])
                .build();
    }
}