package co.edu.uniajc.seguimiento.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TutorshipsStudentsDto implements Serializable {

    @ApiModelProperty(value = "Id Tutoría Estudiante", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idTutorshipsStudents;

    @ApiModelProperty(value = "Primer Nota De Tutoría Estudiante", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double noteOne;

    @ApiModelProperty(value = "Segunda Nota De Tutoría Estudiante", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double noteTwo;

    @ApiModelProperty(value = "Dato Tutoría", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private TutorshipsDto tutorships;

    @ApiModelProperty(value = "Dato Materia", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String subjects;

    @ApiModelProperty(value = "Dato Grupo", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String group;

    @ApiModelProperty(value = "Dato Estudiante", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String students;

    @ApiModelProperty(value = "Dato Promedio De Notas", required = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal averageScore;
}