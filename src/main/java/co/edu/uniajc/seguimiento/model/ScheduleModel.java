package co.edu.uniajc.seguimiento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "schedule")
public class ScheduleModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sc_id")
    private Integer idSchedule;

    @NotNull(message = "Campo Día Obligatorio")
    @Length(min = 5, max = 10, message = "Campo Día Inválido")
    @Column(name = "sc_weekday")
    private String weekday;

    @NotNull(message = "Campo Hora De Inicio Obligatorio")
    @Column(name = "sc_start_time")
    private LocalTime startTime;

    @NotNull(message = "Campo Hora De Fin Obligatorio")
    @Column(name = "sc_end_time")
    private LocalTime endTime;

    @ManyToOne
    @JoinColumn(name = "sc_tutorships", referencedColumnName = "tu_id")
    private TutorshipsModel tutorships;
}