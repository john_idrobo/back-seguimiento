package co.edu.uniajc.seguimiento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "attendance")
public class AttendanceModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "at_id")
    private Integer idAttendance;

    @NotNull(message = "Campo Fecha Y Hora De Asistencia Obligatorio")
    @PastOrPresent(message = "Campo Fecha Y Hora De Asistencia Inválido")
    @Column(name = "at_attendance_date_time")
    private LocalDateTime attendanceDateTime;

    @NotNull(message = "Campo Asistencia Obligatorio")
    @Column(name = "at_attended")
    private Boolean attended;

    @ManyToOne
    @JoinColumn(name = "at_tutorships_students", referencedColumnName = "ts_id")
    private TutorshipsStudentsModel tutorshipsStudents;
}