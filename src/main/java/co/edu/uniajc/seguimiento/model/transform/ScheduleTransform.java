package co.edu.uniajc.seguimiento.model.transform;

import co.edu.uniajc.seguimiento.model.ScheduleModel;
import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import co.edu.uniajc.seguimiento.model.dto.ScheduleDto;
import co.edu.uniajc.seguimiento.model.dto.TutorshipsDto;
import org.springframework.stereotype.Component;

@Component
public class ScheduleTransform {

    public ScheduleModel scheduleDtoToScheduleModel(ScheduleDto schedulesDto) {
        return ScheduleModel.builder()
                .idSchedule(schedulesDto.getIdSchedule())
                .weekday(schedulesDto.getWeekday())
                .startTime(schedulesDto.getStartTime())
                .endTime(schedulesDto.getEndTime())
                .tutorships(TutorshipsModel.builder()
                        .idTutorships(schedulesDto.getTutorships().getIdTutorships())
                        .users(schedulesDto.getTutorships().getUsers())
                        .build())
                .build();
    }

    public ScheduleDto scheduleModelToScheduleDto(ScheduleModel schedulesModel) {
        return ScheduleDto.builder()
                .idSchedule(schedulesModel.getIdSchedule())
                .weekday(schedulesModel.getWeekday())
                .startTime(schedulesModel.getStartTime())
                .endTime(schedulesModel.getEndTime())
                .tutorships(TutorshipsDto.builder()
                        .idTutorships(schedulesModel.getTutorships().getIdTutorships())
                        .users(schedulesModel.getTutorships().getUsers())
                        .build())
                .build();
    }
}