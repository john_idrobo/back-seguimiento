package co.edu.uniajc.seguimiento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tutorships")
public class TutorshipsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tu_id")
    private Integer idTutorships;

    @NotNull(message = "Campo Franja Obligatorio")
    @Length(min = 5, max = 100, message = "Campo Franja Inválido")
    @Column(name = "tu_time_zone")
    private String timeZone;

    @NotNull(message = "Campo Descripción Obligatorio")
    @Length(min = 5, max = 500, message = "Campo Descripción Inválido")
    @Column(name = "tu_description")
    private String description;

    @NotNull(message = "Campo Usuario Obligatorio")
    @Length(min = 6, max = 10, message = "Campo Usuario Inválido")
    @Column(name = "tu_users")
    private String users;
}