package co.edu.uniajc.seguimiento.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PsychologicalStudentsDto implements Serializable {

    @ApiModelProperty(value = "Id Registro Psicológico", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer idPsychologicalStudents;

    @ApiModelProperty(value = "Razón Psicológica", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String reason;

    @ApiModelProperty(value = "Descripción Psicológica", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String description;

    @ApiModelProperty(value = "Factores Psicológicos", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String factors;

    @ApiModelProperty(value = "Antecedentes Psicológicos", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String familyBackground;

    @ApiModelProperty(value = "Diagnóstico Psicológico", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String diagnosis;

    @ApiModelProperty(value = "Datos Usuario", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String users;

    @ApiModelProperty(value = "Datos Estudiante", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String students;

    @ApiModelProperty(value = "Dato Fecha Creación", required = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDate creationDate;
}