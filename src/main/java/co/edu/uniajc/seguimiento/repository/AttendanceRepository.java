package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.model.AttendanceModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

public interface AttendanceRepository extends JpaRepository<AttendanceModel, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "at_id," +
                    "at_attendance_date_time," +
                    "at_attended," +
                    "at_tutorships_students," +
                    "ts_students " +
                    "FROM attendance a " +
                    "INNER JOIN tutorships_students b ON a.at_tutorships_students=b.ts_id " +
                    "INNER JOIN tutorships c ON b.ts_tutorships=c.tu_id " +
                    "WHERE a.at_active='true' AND b.ts_active='true' AND c.tu_finished=:finished AND c.tu_active='true'" +
                    "AND c.tu_id=:tutorships " +
                    "AND c.tu_users=:users " +
                    "ORDER BY at_attendance_date_time DESC")
    List<AttendanceModel> findAllAttendanceByFinishedAndTutorshipsAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "tutorships") Integer tutorships, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "at_id," +
                    "at_attendance_date_time," +
                    "at_attended," +
                    "at_tutorships_students," +
                    "ts_students " +
                    "FROM attendance a " +
                    "INNER JOIN tutorships_students b ON a.at_tutorships_students=b.ts_id " +
                    "INNER JOIN tutorships c ON b.ts_id=c.tu_id " +
                    "WHERE a.at_active='true' AND b.ts_active='true' AND c.tu_finished=:finished AND c.tu_active='true'" +
                    "AND c.tu_id=:tutorships " +
                    "AND b.ts_students=:students " +
                    "AND c.tu_users=:users " +
                    "ORDER BY at_attendance_date_time DESC")
    List<AttendanceModel> findAllAttendanceByFinishedAndTutorshipsAndStudentsAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "tutorships") Integer tutorships, @Param(value = "students") String students, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "at_id," +
                    "at_attendance_date_time," +
                    "at_attended," +
                    "at_tutorships_students " +
                    "FROM attendance a " +
                    "INNER JOIN tutorships_students b ON a.at_tutorships_students=b.ts_id " +
                    "INNER JOIN tutorships c ON b.ts_tutorships=c.tu_id " +
                    "INNER JOIN schedule d ON c.tu_id=d.sc_tutorships " +
                    "WHERE a.at_id<>:id " +
                    "AND a.at_active='true' AND b.ts_active='true' AND c.tu_finished='false' " +
                    "AND c.tu_active='true' AND d.sc_active='true' " +
                    "AND a.at_attendance_date_time BETWEEN CAST(:start_date AS timestamp) AND CAST(:end_date AS timestamp)" +
                    "AND b.ts_id=:tutorships_students " +
                    "AND b.ts_students=:students")
    List<AttendanceModel> existsAttendanceAllByIdAndStartDateTimeAndEndDateTimeAndtutorshipsStudentsAndStudents(@Param(value = "id") Integer id, @Param(value = "start_date") LocalDateTime start_date, @Param(value = "end_date") LocalDateTime end_date, @Param(value = "tutorships_students") Integer tutorships_students, @Param(value = "students") String students);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "UPDATE attendance " +
                    "SET at_active='false'" +
                    "WHERE at_id=:id")
    int inactiveAttendanceById(@Param(value = "id") Integer id);
}