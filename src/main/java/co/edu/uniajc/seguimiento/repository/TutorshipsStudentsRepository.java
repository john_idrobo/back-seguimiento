package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.model.TutorshipsStudentsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface TutorshipsStudentsRepository extends JpaRepository<TutorshipsStudentsModel, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_id," +
                    "ts_note_one," +
                    "ts_note_two," +
                    "ts_tutorships," +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id  " +
                    "WHERE a.ts_active='true'" +
                    "AND b.tu_finished=:finished " +
                    "AND b.tu_active='true' " +
                    "ORDER BY a.ts_id DESC")
    List<TutorshipsStudentsModel> findAllTutorshipsStudentsByFinished(@Param(value = "finished") Boolean finished);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_id," +
                    "ts_note_one," +
                    "ts_note_two," +
                    "ts_tutorships," +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id  " +
                    "WHERE a.ts_active='true'" +
                    "AND b.tu_finished=:finished " +
                    "AND b.tu_active='true' " +
                    "AND a.ts_students=:students " +
                    "ORDER BY a.ts_id DESC")
    List<TutorshipsStudentsModel> findAllTutorshipsStudentsByFinishedAndStudents(@Param(value = "finished") Boolean finished, @Param(value = "students") String students);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_id," +
                    "ts_note_one," +
                    "ts_note_two," +
                    "ts_tutorships," +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id  " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished=:finished " +
                    "AND b.tu_active='true' " +
                    "AND b.tu_id=:tutorships " +
                    "AND b.tu_users=:users " +
                    "ORDER BY a.ts_id DESC")
    List<TutorshipsStudentsModel> findAllTutorshipsStudentsByFinishedAndTutorshipsAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "tutorships") Integer tutorships, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_id," +
                    "ts_note_one," +
                    "ts_note_two," +
                    "ts_tutorships," +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id  " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished=:finished " +
                    "AND b.tu_active='true' " +
                    "AND b.tu_id=:tutorships " +
                    "AND a.ts_students=:students " +
                    "AND b.tu_users=:users " +
                    "ORDER BY a.ts_id DESC")
    List<TutorshipsStudentsModel> findAllTutorshipsStudentsByFinishedAndTutorshipsAndStudentsAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "tutorships") Integer tutorships, @Param(value = "students") String students, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_id," +
                    "ts_note_one," +
                    "ts_note_two," +
                    "ts_tutorships," +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id  " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished=:finished " +
                    "AND b.tu_active='true' " +
                    "AND a.ts_subjects=:subjects " +
                    "AND b.tu_id=:tutorships " +
                    "AND b.tu_users=:users " +
                    "ORDER BY a.ts_id DESC")
    List<TutorshipsStudentsModel> findAllTutorshipsStudentsByFinishedAndSubjectsAndTutorshipsAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "subjects") String subjects, @Param(value = "tutorships") Integer tutorships, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_id," +
                    "ts_note_one," +
                    "ts_note_two," +
                    "ts_tutorships," +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id  " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished=:finished " +
                    "AND b.tu_active='true' " +
                    "AND a.ts_group=:group " +
                    "AND b.tu_id=:tutorships " +
                    "AND b.tu_users=:users " +
                    "ORDER BY a.ts_id DESC")
    List<TutorshipsStudentsModel> findAllTutorshipsStudentsByFinishedAndGroupAndTutorshipsAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "group") String group, @Param(value = "tutorships") Integer tutorships, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "COUNT(*)" +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id  " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished='false' " +
                    "AND b.tu_active='true' " +
                    "AND a.ts_group=:group " +
                    "AND a.ts_subjects=:subjects " +
                    "AND a.ts_students=:students")
    int existsTutorshipsStudentsByGroupAndSubjectsAndStudents(@Param(value = "group") String group, @Param(value = "subjects") String subjects, @Param(value = "students") String students);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "COUNT(*)" +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id  " +
                    "WHERE a.ts_active='true' " +
                    "AND a.ts_id<>:id " +
                    "AND b.tu_finished='false' " +
                    "AND b.tu_active='true' " +
                    "AND a.ts_group=:group " +
                    "AND a.ts_subjects=:subjects " +
                    "AND a.ts_students=:students")
    int existsTutorshipsStudentsByIdAndGroupAndSubjectsAndStudents(@Param(value = "id") Integer id, @Param(value = "group") String group, @Param(value = "subjects") String subjects, @Param(value = "students") String students);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "COUNT(*)" +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id  " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished='false' " +
                    "AND b.tu_active='true' " +
                    "AND b.tu_id=:tutorships " +
                    "AND a.ts_students=:students")
    int existsTutorshipsStudentsByTutorshipsAndStudents(@Param(value = "tutorships") Integer tutorships, @Param(value = "students") String students);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "UPDATE tutorships_students " +
                    "SET ts_active='false'" +
                    "WHERE ts_id=:id")
    int inactiveTutorshipsStudentsById(@Param(value = "id") Integer id);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students, " +
                    "ts_note_one, " +
                    "ts_note_two " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished='false' " +
                    "AND b.tu_active='true'" +
                    "AND b.tu_id=:tutorships " +
                    "AND a.ts_students=:students " +
                    "AND b.tu_users=:users")
    List<Object[]> reportTutorshipsStudentsByTutorshipsAndStudentsAndUsers(@Param(value = "tutorships") Integer tutorships, @Param(value = "students") String students, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students, " +
                    "ROUND(CAST(((ts_note_one+ts_note_two)/'2') AS DECIMAL), '1') AS ts_average " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished='true' " +
                    "AND b.tu_active='true'" +
                    "AND b.tu_id=:tutorships " +
                    "AND a.ts_students=:students " +
                    "AND b.tu_users=:users")
    List<Object[]> reportAverageTutorshipsStudentsByTutorshipsAndStudentsAndUsers(@Param(value = "tutorships") Integer tutorships, @Param(value = "students") String students, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students, " +
                    "ts_note_one, " +
                    "ts_note_two " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished='false' " +
                    "AND b.tu_active='true'" +
                    "AND b.tu_id=:tutorships " +
                    "AND b.tu_users=:users")
    List<Object[]> reportTutorshipsStudentsByTutorshipsAndUsers(@Param(value = "tutorships") Integer tutorships, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ts_subjects, " +
                    "ts_group, " +
                    "ts_students, " +
                    "ROUND(CAST(((ts_note_one+ts_note_two)/'2') AS DECIMAL), '1') AS ts_average " +
                    "FROM tutorships_students a " +
                    "INNER JOIN tutorships b " +
                    "ON a.ts_tutorships=b.tu_id " +
                    "WHERE a.ts_active='true' " +
                    "AND b.tu_finished='true' " +
                    "AND b.tu_active='true'" +
                    "AND b.tu_id=:tutorships " +
                    "AND b.tu_users=:users")
    List<Object[]> reportAverageTutorshipsStudentsByTutorshipsAndUsers(@Param(value = "tutorships") Integer tutorships, @Param(value = "users") String users);
}