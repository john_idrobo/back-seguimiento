package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.model.ActivitiesStudentsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface ActivitiesStudentsRepository extends JpaRepository<ActivitiesStudentsModel, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "as_id," +
                    "as_attendance_date," +
                    "as_completed_hours," +
                    "as_activities, " +
                    "as_students " +
                    "FROM activities_students " +
                    "WHERE as_active='true'" +
                    "ORDER BY as_id DESC")
    List<ActivitiesStudentsModel> findAllActivitiesStudents();

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "as_id," +
                    "as_attendance_date," +
                    "as_completed_hours," +
                    "as_activities, " +
                    "as_students " +
                    "FROM activities_students " +
                    "WHERE as_active='true'" +
                    "AND as_attendance_date=:date " +
                    "ORDER BY as_id DESC")
    List<ActivitiesStudentsModel> findAllActivitiesStudentsByDate(@Param(value = "date") LocalDate date);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "as_id," +
                    "as_attendance_date," +
                    "as_completed_hours," +
                    "as_activities, " +
                    "as_students " +
                    "FROM activities_students " +
                    "WHERE as_active='true'" +
                    "AND as_activities=:activities " +
                    "ORDER BY as_id DESC")
    List<ActivitiesStudentsModel> findAllActivitiesStudentsByActivities(@Param(value = "activities") Integer activities);


    @Query(nativeQuery = true,
            value = "SELECT " +
                    "as_id," +
                    "as_attendance_date," +
                    "as_completed_hours," +
                    "as_activities, " +
                    "as_students " +
                    "FROM activities_students " +
                    "WHERE as_active='true'" +
                    "AND as_students=:students " +
                    "ORDER BY as_id DESC")
    List<ActivitiesStudentsModel> findAllActivitiesStudentsByStudents(@Param(value = "students") String students);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "COUNT(*)" +
                    "FROM activities_students " +
                    "WHERE as_active='true'" +
                    "AND as_attendance_date=:date " +
                    "AND as_activities=:activities " +
                    "AND as_students=:students")
    int existsActivitiesStudentsByDateAndActivitiesAndStudents(@Param(value = "date") LocalDate date, @Param(value = "activities") Integer activities, @Param(value = "students") String students);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "UPDATE activities_students " +
                    "SET as_active='false'" +
                    "WHERE as_id=:id")
    int inactiveActivitiesStudentsById(@Param(value = "id") Integer id);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "as_activities, " +
                    "as_students, " +
                    "SUM(as_completed_hours) AS as_completed_hours " +
                    "FROM activities_students " +
                    "WHERE as_active='true'" +
                    "GROUP BY as_activities, as_students " +
                    "ORDER BY as_completed_hours DESC")
    List<Object[]> reportAllActivitiesStudents();

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "as_activities, " +
                    "as_students, " +
                    "SUM(as_completed_hours) AS as_completed_hours " +
                    "FROM activities_students " +
                    "WHERE as_active='true'" +
                    "AND as_students=:students " +
                    "GROUP BY as_activities, as_students " +
                    "ORDER BY as_completed_hours DESC")
    List<Object[]> reportAllActivitiesStudentsByStudents(@Param(value = "students") String students);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "as_activities, " +
                    "as_students, " +
                    "SUM(as_completed_hours) AS as_completed_hours " +
                    "FROM activities_students " +
                    "WHERE as_active='true'" +
                    "AND as_activities=:activities " +
                    "GROUP BY as_activities, as_students " +
                    "ORDER BY as_completed_hours DESC")
    List<Object[]> reportAllActivitiesStudentsByActivities(@Param(value = "activities") Integer activities);
}