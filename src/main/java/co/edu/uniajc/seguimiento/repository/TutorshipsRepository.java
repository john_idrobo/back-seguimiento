package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.model.TutorshipsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface TutorshipsRepository extends JpaRepository<TutorshipsModel, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "tu_id," +
                    "tu_time_zone," +
                    "tu_description," +
                    "tu_users " +
                    "FROM tutorships " +
                    "WHERE tu_finished=:finished " +
                    "AND tu_active='true'" +
                    "ORDER BY tu_id DESC")
    List<TutorshipsModel> findAllTutorshipsByFinished(@Param(value = "finished") Boolean finished);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "tu_id," +
                    "tu_time_zone," +
                    "tu_description," +
                    "tu_users " +
                    "FROM tutorships " +
                    "WHERE tu_finished=:finished " +
                    "AND tu_active='true'" +
                    "AND tu_id=:id " +
                    "ORDER BY tu_id DESC")
    List<TutorshipsModel> findTutorshipsByFinishedAndId(@Param(value = "finished") Boolean finished, @Param(value = "id") Integer id);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "tu_id," +
                    "tu_time_zone," +
                    "tu_description," +
                    "tu_users " +
                    "FROM tutorships " +
                    "WHERE tu_finished=:finished " +
                    "AND tu_active='true'" +
                    "AND tu_users=:users " +
                    "ORDER BY tu_id DESC")
    List<TutorshipsModel> findAllTutorshipsByFinishedAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "tu_id," +
                    "tu_time_zone," +
                    "tu_description," +
                    "tu_users " +
                    "FROM tutorships " +
                    "WHERE tu_finished=:finished " +
                    "AND tu_active='true'" +
                    "AND tu_creation_date=:date " +
                    "ORDER BY tu_id DESC")
    List<TutorshipsModel> findAllTutorshipsByFinishedAndDate(@Param(value = "finished") Boolean finished, @Param(value = "date") LocalDate date);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "tu_id," +
                    "tu_time_zone," +
                    "tu_description," +
                    "tu_users " +
                    "FROM tutorships " +
                    "WHERE tu_finished=:finished " +
                    "AND tu_active='true'" +
                    "AND tu_time_zone=:zone " +
                    "ORDER BY tu_id DESC")
    List<TutorshipsModel> findAllTutorshipsByFinishedAndZone(@Param(value = "finished") Boolean finished, @Param(value = "zone") String zone);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "tu_id," +
                    "tu_time_zone," +
                    "tu_description," +
                    "tu_users " +
                    "FROM tutorships " +
                    "WHERE tu_finished=:finished " +
                    "AND tu_active='true'" +
                    "AND tu_creation_date=:date " +
                    "AND tu_users=:users " +
                    "ORDER BY tu_id DESC")
    List<TutorshipsModel> findAllTutorshipsByFinishedAndDateAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "date") LocalDate date, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "tu_id," +
                    "tu_time_zone," +
                    "tu_description," +
                    "tu_users " +
                    "FROM tutorships " +
                    "WHERE tu_finished=:finished " +
                    "AND tu_active='true'" +
                    "AND tu_time_zone=:zone " +
                    "AND tu_users=:users " +
                    "ORDER BY tu_id DESC")
    List<TutorshipsModel> findAllTutorshipsByFinishedAndZoneAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "zone") String zone, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT COUNT(*) " +
                    "FROM tutorships " +
                    "WHERE tu_finished='false'" +
                    "AND tu_active='true' " +
                    "AND tu_id=:id " +
                    "AND tu_users=:users")
    int existsTutorshipsByIdAndUsers(@Param(value = "id") Integer id, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "COUNT(*)" +
                    "FROM tutorships " +
                    "WHERE tu_active='true' " +
                    "AND tu_finished='false' " +
                    "AND tu_time_zone=:zone " +
                    "AND tu_description=:description " +
                    "AND tu_users=:users")
    int existsTutorshipsByZoneAndDescriptionAndUsers(@Param(value = "zone") String zone, @Param(value = "description") String description, @Param(value = "users") String users);

    @Procedure(procedureName = "inactiveTutorshipsById")
    int inactiveTutorshipsById(@Param(value = "id") Integer id);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "UPDATE tutorships " +
                    "SET tu_finished='true'" +
                    "WHERE tu_id=:id")
    int finishedTutorshipsById(@Param(value = "id") Integer id);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "tu_creation_date," +
                    "tu_time_zone, " +
                    "tu_description," +
                    "tu_users " +
                    "FROM tutorships a " +
                    "INNER JOIN tutorships_students b " +
                    "ON a.tu_id=b.ts_tutorships " +
                    "WHERE b.ts_active='true' " +
                    "AND a.tu_finished=:finished " +
                    "AND a.tu_active='true'" +
                    "AND a.tu_id=:id " +
                    "AND a.tu_users=:users")
    List<Object[]> reportTutorshipsByFinishedAndIdAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "id") Integer id, @Param(value = "users") String users);
}