package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.model.PsychologicalStudentsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface PsychologicalStudentsRepository extends JpaRepository<PsychologicalStudentsModel, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ps_id," +
                    "ps_creation_date," +
                    "ps_reason," +
                    "ps_description," +
                    "ps_factors," +
                    "ps_family_background," +
                    "ps_diagnosis," +
                    "ps_users," +
                    "ps_students " +
                    "FROM psychological_students " +
                    "WHERE ps_active='true'" +
                    "ORDER BY ps_id DESC")
    List<Object[]> findAllPsychologicalStudents();

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ps_id," +
                    "ps_creation_date," +
                    "ps_reason," +
                    "ps_description," +
                    "ps_factors," +
                    "ps_family_background," +
                    "ps_diagnosis," +
                    "ps_users," +
                    "ps_students " +
                    "FROM psychological_students " +
                    "WHERE ps_active='true'" +
                    "AND ps_users=:users " +
                    "ORDER BY ps_id DESC")
    List<Object[]> findAllPsychologicalStudentsByUsers(@Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ps_id," +
                    "ps_creation_date," +
                    "ps_reason," +
                    "ps_description," +
                    "ps_factors," +
                    "ps_family_background," +
                    "ps_diagnosis," +
                    "ps_users," +
                    "ps_students " +
                    "FROM psychological_students " +
                    "WHERE ps_active='true'" +
                    "AND ps_students=:students " +
                    "ORDER BY ps_id DESC")
    List<Object[]> findAllPsychologicalStudentsByStudents(@Param(value = "students") String students);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ps_id," +
                    "ps_creation_date," +
                    "ps_reason," +
                    "ps_description," +
                    "ps_factors," +
                    "ps_family_background," +
                    "ps_diagnosis," +
                    "ps_users," +
                    "ps_students " +
                    "FROM psychological_students " +
                    "WHERE ps_active='true'" +
                    "AND ps_creation_date=:date " +
                    "ORDER BY ps_id DESC")
    List<Object[]> findAllPsychologicalStudentsByDate(@Param(value = "date") LocalDate date);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ps_id," +
                    "ps_creation_date," +
                    "ps_reason," +
                    "ps_description," +
                    "ps_factors," +
                    "ps_family_background," +
                    "ps_diagnosis," +
                    "ps_users," +
                    "ps_students " +
                    "FROM psychological_students " +
                    "WHERE ps_active='true'" +
                    "AND ps_creation_date=:date " +
                    "AND ps_users=:users " +
                    "ORDER BY ps_id DESC")
    List<Object[]> findAllPsychologicalStudentsByDateAndUsers(@Param(value = "date") LocalDate date, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ps_id," +
                    "ps_creation_date," +
                    "ps_reason," +
                    "ps_description," +
                    "ps_factors," +
                    "ps_family_background," +
                    "ps_diagnosis," +
                    "ps_users," +
                    "ps_students " +
                    "FROM psychological_students " +
                    "WHERE ps_active='true'" +
                    "AND ps_students=:students " +
                    "AND ps_users=:users " +
                    "ORDER BY ps_id DESC")
    List<Object[]> findAllPsychologicalStudentsByStudentsAndUsers(@Param(value = "students") String students, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "COUNT(*)" +
                    "FROM psychological_students " +
                    "WHERE ps_active='true'" +
                    "AND ps_creation_date=CURRENT_DATE " +
                    "AND ps_students=:students " +
                    "AND ps_users=:users")
    int existsPsychologicalStudentsByStudentsAndUsers(@Param(value = "students") String students, @Param(value = "users") String users);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "UPDATE psychological_students " +
                    "SET ps_active='false'" +
                    "WHERE ps_id=:id")
    int inactivePsychologicalStudentsById(@Param(value = "id") Integer id);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "ps_id," +
                    "ps_creation_date," +
                    "ps_reason," +
                    "ps_description," +
                    "ps_factors," +
                    "ps_family_background," +
                    "ps_diagnosis," +
                    "ps_users," +
                    "ps_students " +
                    "FROM psychological_students " +
                    "WHERE ps_active='true'" +
                    "AND ps_id=:id " +
                    "AND ps_students=:students " +
                    "AND ps_users=:users")
    List<Object[]> reportPsychologicalStudentsByIdAndStudentsAndUsers(@Param(value = "id") Integer id, @Param(value = "students") String students, @Param(value = "users") String users);
}