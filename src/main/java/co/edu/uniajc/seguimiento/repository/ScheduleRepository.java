package co.edu.uniajc.seguimiento.repository;

import co.edu.uniajc.seguimiento.model.ScheduleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<ScheduleModel, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "sc_id," +
                    "sc_weekday," +
                    "sc_start_time," +
                    "sc_end_time," +
                    "sc_tutorships " +
                    "FROM schedule a " +
                    "INNER JOIN tutorships b ON a.sc_tutorships=b.tu_id " +
                    "WHERE a.sc_active='true' AND b.tu_active='true' AND b.tu_finished=:finished " +
                    "AND a.sc_tutorships=:tutorships " +
                    "AND b.tu_users=:users " +
                    "ORDER BY a.sc_id DESC")
    List<ScheduleModel> findScheduleByFinishedAndTutorshipsAndUsers(@Param(value = "finished") Boolean finished, @Param(value = "tutorships") Integer tutorships, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "sc_id," +
                    "sc_weekday," +
                    "sc_start_time," +
                    "sc_end_time," +
                    "sc_tutorships " +
                    "FROM schedule a " +
                    "INNER JOIN tutorships b ON a.sc_tutorships=b.tu_id " +
                    "INNER JOIN tutorships_students c ON b.tu_id=c.ts_tutorships " +
                    "WHERE a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false' AND c.ts_active='true'" +
                    "AND c.ts_students=:students")
    List<ScheduleModel> findScheduleByStudents(@Param(value = "students") String students);

    @Query(nativeQuery = true,
            value = "SELECT a.sc_weekday, a.sc_start_time, a.sc_end_time, c.ts_students FROM schedule a " +
                    "INNER JOIN tutorships b ON a.sc_tutorships=b.tu_id " +
                    "INNER JOIN tutorships_students c ON b.tu_id=c.ts_tutorships " +
                    "WHERE a.sc_tutorships<>:tutorships " +
                    "AND a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false' AND c.ts_active='true'" +
                    "AND c.ts_students=Any(SELECT ts_students FROM tutorships_students d " +
                    "INNER JOIN tutorships e ON e.tu_id=d.ts_tutorships " +
                    "WHERE d.ts_active='true' AND e.tu_active='true' AND e.tu_finished='false' AND d.ts_tutorships=:tutorships) " +
                    "AND a.sc_weekday=:weekday AND a.sc_start_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "AND a.sc_end_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "OR " +
                    "a.sc_tutorships<>:tutorships " +
                    "AND a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false' AND c.ts_active='true'" +
                    "AND c.ts_students=Any(SELECT ts_students FROM tutorships_students d " +
                    "INNER JOIN tutorships e ON e.tu_id=d.ts_tutorships " +
                    "WHERE d.ts_active='true' AND e.tu_active='true' AND e.tu_finished='false' AND d.ts_tutorships=:tutorships) " +
                    "AND a.sc_weekday=:weekday AND a.sc_start_time NOT BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "AND a.sc_end_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "OR " +
                    "a.sc_tutorships<>:tutorships " +
                    "AND a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false' AND c.ts_active='true'" +
                    "AND c.ts_students=Any(SELECT ts_students FROM tutorships_students d " +
                    "INNER JOIN tutorships e ON e.tu_id=d.ts_tutorships " +
                    "WHERE d.ts_active='true' AND e.tu_active='true' AND e.tu_finished='false' AND d.ts_tutorships=:tutorships) " +
                    "AND a.sc_weekday=:weekday AND a.sc_start_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "AND a.sc_end_time NOT BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "ORDER BY a.sc_start_time, a.sc_end_time ASC")
    List<String> existsScheduleByWeekdayAndStartAndEndAndTutorships(@Param(value = "weekday") String weekday, @Param(value = "start") LocalTime start, @Param(value = "end") LocalTime end, @Param(value = "tutorships") Integer tutorships);

    @Query(nativeQuery = true,
            value = "SELECT a.sc_weekday, a.sc_start_time, a.sc_end_time FROM schedule a " +
                    "INNER JOIN tutorships b ON a.sc_tutorships=b.tu_id " +
                    "INNER JOIN tutorships_students c ON b.tu_id=c.ts_tutorships " +
                    "WHERE " +
                    "a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false' AND c.ts_active='true'" +
                    "AND c.ts_students=:students " +
                    "AND a.sc_weekday=:weekday AND a.sc_start_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "AND a.sc_end_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "OR " +
                    "a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false' AND c.ts_active='true'" +
                    "AND c.ts_students=:students " +
                    "AND a.sc_weekday=:weekday AND a.sc_start_time NOT BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "AND a.sc_end_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "OR " +
                    "a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false' AND c.ts_active='true'" +
                    "AND c.ts_students=:students " +
                    "AND a.sc_weekday=:weekday AND a.sc_start_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "AND a.sc_end_time NOT BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "ORDER BY a.sc_start_time, a.sc_end_time ASC")
    List<String> existsScheduleByWeekdayAndStartAndEndAndStudents(@Param(value = "weekday") String weekday, @Param(value = "start") LocalTime start, @Param(value = "end") LocalTime end, @Param(value = "students") String students);

    @Query(nativeQuery = true,
            value = "SELECT sc_weekday, sc_start_time, sc_end_time FROM schedule a " +
                    "INNER JOIN tutorships b ON a.sc_tutorships=b.tu_id " +
                    "WHERE a.sc_id<>:id " +
                    "AND a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false'" +
                    "AND b.tu_users=:users " +
                    "AND a.sc_weekday=:weekday AND a.sc_start_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "AND a.sc_end_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "OR " +
                    "a.sc_id<>:id " +
                    "AND a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false'" +
                    "AND b.tu_users=:users " +
                    "AND a.sc_weekday=:weekday AND a.sc_start_time NOT BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "AND a.sc_end_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "OR " +
                    "a.sc_id<>:id " +
                    "AND a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false'" +
                    "AND b.tu_users=:users " +
                    "AND a.sc_weekday=:weekday AND a.sc_start_time BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "AND a.sc_end_time NOT BETWEEN CAST(:start AS TIME) AND CAST(:end AS TIME)" +
                    "ORDER BY a.sc_start_time, a.sc_end_time ASC")
    List<String> existsScheduleByIdAndWeekdayAndStartAndEndAndUsers(@Param(value = "id") Integer id, @Param(value = "weekday") String weekday, @Param(value = "start") LocalTime start, @Param(value = "end") LocalTime end, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "sc_id," +
                    "sc_weekday," +
                    "sc_start_time," +
                    "sc_end_time," +
                    "sc_tutorships " +
                    "FROM schedule a " +
                    "INNER JOIN tutorships b ON a.sc_tutorships=b.tu_id " +
                    "INNER JOIN tutorships_students c ON b.tu_id=c.ts_tutorships " +
                    "WHERE a.sc_active='true' AND b.tu_active='true' " +
                    "AND b.tu_finished='false' AND c.ts_active='true' " +
                    "AND c.ts_id=:tutorships_students " +
                    "AND c.ts_students=:students " +
                    "AND a.sc_weekday=:weekday AND CAST(:start AS TIME) BETWEEN a.sc_start_time AND a.sc_end_time")
    ScheduleModel existsScheduleByStartAndWeekdayAndtutorshipsStudentsAndStudents(@Param(value = "start") LocalTime start, @Param(value = "weekday") String weekday, @Param(value = "tutorships_students") Integer tutorships_students, @Param(value = "students") String students);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "COUNT(*)" +
                    "FROM schedule a " +
                    "INNER JOIN tutorships b ON a.sc_tutorships=b.tu_id " +
                    "WHERE a.sc_active='true' AND b.tu_active='true' AND b.tu_finished='false'" +
                    "AND a.sc_tutorships=:tutorships " +
                    "AND b.tu_users=:users")
    int findScheduleByTutorshipsAndUsers(@Param(value = "tutorships") Integer tutorships, @Param(value = "users") String users);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "COUNT(*)" +
                    "FROM schedule a " +
                    "INNER JOIN tutorships b ON a.sc_tutorships=b.tu_id " +
                    "INNER JOIN tutorships_students c ON b.tu_id=c.ts_tutorships " +
                    "WHERE a.sc_active='true' AND b.tu_active='true' " +
                    "AND b.tu_finished='false' AND c.ts_active='true' " +
                    "AND c.ts_id=:tutorships_students " +
                    "AND c.ts_students=:students")
    int existsScheduleBytutorshipsStudentsAndStudents(@Param(value = "tutorships_students") Integer tutorships_students, @Param(value = "students") String students);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "UPDATE schedule " +
                    "SET sc_active='false'" +
                    "WHERE sc_id=:id")
    int inactiveScheduleById(@Param(value = "id") Integer id);
}