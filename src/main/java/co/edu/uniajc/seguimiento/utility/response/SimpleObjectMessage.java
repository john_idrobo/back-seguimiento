package co.edu.uniajc.seguimiento.utility.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class SimpleObjectMessage {

    @ApiModelProperty(value = "Code Status", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer code;

    @ApiModelProperty(value = "Message Status", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    @ApiModelProperty(value = "Description Exception", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object description;

    @ApiModelProperty(value = "Description Path", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String path;
}