package co.edu.uniajc.seguimiento.utility.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SimpleObjectResponse {

    @ApiModelProperty(value = "Code Status", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer code;

    @ApiModelProperty(value = "Message Status", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    @ApiModelProperty(value = "Data JSON", required = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object data;
}